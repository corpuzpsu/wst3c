@if (isset(Auth::user()->username))
    <script>window.location = "/admin/dashboard"</script>
@endif
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ url('../css/style.css') }}">
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://kit.fontawesome.com/6d6b82be0b.js" crossorigin="anonymous"></script>
    <script type='text/javascript' src=''></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>User - Search</title>
    
</head>
<body>
<div class="justify-content-between align-items-center shadow nav">
    <div>
        <h2 style="margin: 0">Search Certificate</h2>
    </div>

    <div class="d-flex flex-row">

        <div class="p-2">
            <a class="nav-link" href="/admin/login" style="color: white;">Signin</a>
        </div>
    </div>
</div>


<div class="search">       
    <form action="user/searchCert" method="post" enctype="multipart/form-data"> 
        @csrf
        <div class="row mt-5 p-5"><center>
            <div class="col-sm-4">
                <label class="form-label">Code</label>
                <input type="text" class="form-control" name="code" id="colFormLabel" value = "{{ old ('code') }}" placeholder="Search By Certificate ID">
            </div>

            <div class="col-sm-4 mt-3">
                <button type="submit" class="btn btn-primary" name="save" >Search</button> </center>
            </div>
        </div>
    </form>
</div>


   


<!-- <footer class="page-footer mb-0 font-small justify-content-between align-items-center shadow nav sticky-bottom">


  <div class="footer-copyright text-center py-3">©{{ Date("Y") }} Copyright: PSU
    <a href="/" class="text-white"> </a>
  </div>

</footer> -->
</body>
</html>