<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>404 Custom Error Page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    
</head>
<body>
    <div class="container-lg mt-5 pt-5">
        <div class="alert alert-danger text-center">
            <h2 class="display-3"><b>404</b></h2>
            <p class="display-5">Ooops! Something is wrong.</p>
        </div>
    </div>
</body>
</html>