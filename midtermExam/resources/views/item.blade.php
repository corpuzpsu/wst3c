<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Item Page</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body>
    <div class="container my-5 py-5">
        <div class="row mb-5">
            <div class="col-12">
                <h3 class="fw-bold text-center">Corpuz, Jackelyn N. (BSIT-3C)</h3>
                <hr>
            </div>
        </div>
        <div class="col-md-6 mx-auto">
            <form action="">
                <div class="mb-3">
                    <label for="item_id" class="col-form-label"><b>Item ID: </b>{{ $item[0] }}</label>
                </div>
                <div class="mb-3">
                    <label for="name" class="form-label"><b>Name: </b>{{ $item[1] }}</label>
                </div>
                <div class="mb-3">
                    <label for="price" class="form-label"><b>Price: </b>{{ $item[2] }}</label>
                </div>
            </form>
        </div>
    </div>
</body>
</html>