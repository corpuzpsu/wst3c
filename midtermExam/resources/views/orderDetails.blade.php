<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Details</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body>
    <div class="container my-4 py-4">
        <div class="row mb-5">
            <div class="col-12">
                <h3 class="fw-bold text-center">Corpuz, Jackelyn N. (BSIT-3C)</h3>
                <hr>
            </div>
        </div>
        <div class="col-md-6 mx-auto">
            <form action="">
                <div class="mb-3">
                    <label class="form-label"><b>Transaction No: </b>{{ $orderDetails[0] }}</label>
                </div>
                <div class="mb-3 ">
                    <label class="form-label"><b>Order No: </b>{{ $orderDetails[1] }}</label>
                </div>
                <div class="mb-3">
                    <label class="form-label"><b>Item ID: </b>{{ $orderDetails[2] }}</label>
                </div>
                <div class="mb-3">
                    <label class="form-label"><b>Name: </b>{{ $orderDetails[3] }}</label>
                </div>
                <div class="mb-3">
                    <label class="form-label"><b>Price: </b>{{ $orderDetails[4] }}</label>
                </div>
                <div class="mb-3">
                    <label class="form-label"><b>Quantity: </b>{{ $orderDetails[5] }}</label>
                </div>
                <div class="mb-3">
                    <label class="form-label"><b>Receipt No: </b>{{ $orderDetails[6] }}</label>
                </div>
            </form>
        </div>
    </div>
</body>
</html>