<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/customer/{cust_id}/{name}/{address}/{age?}', function($cust_id,$name,$address,$age=null)
{
    $customer = [$cust_id, $name, $address, $age];
    return view('customer', ['customer' => $customer]);
});

Route::get('/item/{item_id}/{name}/{price}', function($item_id,$name,$price)
{
    $item = [$item_id, $name, $price];
    return view('item', ['item' => $item]);
});

Route::get('/order/{cust_id}/{name}/{order_no}/{date}', function($cust_id,$name,$order_no,$date)
{
    $order = [$cust_id, $name, $order_no, $date];
    return view('order', ['order' => $order]);
});

Route::get('/orderDetails/{trans_no}/{order_no}/{item_id}/{name}/{price}/{qty}/{receipt_no?}', 
function($trans_no,$order_no,$item_id,$name,$price,$qty,$receipt_no=null)
{
    $orderDetails = [$trans_no, $order_no, $item_id, $name, $price, $qty, $receipt_no];
    return view('orderDetails', ['orderDetails' => $orderDetails]);
});

/*Route::get('customer/details', function(){
    $url = "<a>Customer Page <br> Corpuz, Jackelyn N. (BSIT-3C)</a>";
    return $url;
})->name('customer.details');


Route::get('item/details', function(){
    $url = "<a>Item Page <br> Corpuz, Jackelyn N. (BSIT-3C)</a>";
    return $url;
})->name('item.details');

Route::get('order/details', function(){
    $url = "<a>Order Page <br> Corpuz, Jackelyn N. (BSIT-3C)</a>";
    return $url;
})->name('order.details');

Route::get('details/details', function(){
    $url = "<a>Order Details Page <br> Corpuz, Jackelyn N. (BSIT-3C)</a>";
    return $url;
})->name('details.details');*/

