<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
   function customerData($cust_id, $name, $address)
   {
        $customer = [$cust_id, $name, $address,];
        return view('customer', ['customer' => $customer]);
   }

   function itemData($item_id,$name,$price)
   {
        $item = [$item_id, $name, $price];
        return view('item', ['item' => $item]);
   }

   function orderData($cust_id,$name,$order_no,$date)
   {
        $order = [$cust_id, $name, $order_no, $date];
        return view('order', ['order' => $order]);
   }

   function orderDetails($trans_no,$order_no,$item_id,$name,$price,$qty)
   {
        $orderDetails = [$trans_no, $order_no, $item_id, $name, $price, $qty];
        return view('orderDetails', ['orderDetails' => $orderDetails]);
   }
}
