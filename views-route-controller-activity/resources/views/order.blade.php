<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Page</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body>
    <div class="container my-5 py-5">
        <div class="row mb-5">
            <div class="col-12">
                <h3 class="fw-bold text-center">Corpuz, Jackelyn N. (BSIT-3C)</h3>
                <hr>
            </div>
        </div>
        <div class="col-md-6 mx-auto">
            <form action="">
                <div class="mb-3">
                    <label class="col-form-label"><b>Customer ID: </b></label>
                    <input class="form-control" type="text" value="{{ $order[0] }}" aria-label="Disabled input example" disabled readonly>
                </div>
                
                <div class="mb-3">
                    <label class="form-label"><b>Name: </b></label>
                    <input class="form-control" type="text" value="{{ $order[1] }}" aria-label="Disabled input example" disabled readonly>
                </div>
                
                <div class="mb-3">
                    <label class="form-label"><b>Order No: </b></label>
                    <input class="form-control" type="text" value="{{ $order[2] }}" aria-label="Disabled input example" disabled readonly>
                </div>
                
                <div class="mb-3">
                    <label class="form-label"><b>Date: </b></label>
                    <input class="form-control" type="text" value="{{ $order[3] }}" aria-label="Disabled input example" disabled readonly>
                </div>
                
            </form>
        </div>
    </div>
</body>
</html>