@extends('layouts.app')

@section('content')
<section id="home">
    <div class="container mb-5">
        <div class="row">
        
            <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                </div>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{{ asset('images/bg2.JPG') }}" class="d-block w-100" alt="..." height="550px">
                        <div class="carousel-caption d-none d-md-block">
                            <p class="lead fw-bold text-warning">"Every great design begins with an even better story."</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="{{ asset('images/bg1.JPG') }}" class="d-block w-100" alt="..."  height="550px">
                        <div class="carousel-caption d-none d-md-block">
                            <p class="lead fw-bold text-warning">"Be creative and have fun."</p>
                        </div>
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- About Section -->
<section id="about">
    <div class="container my-4 py-4">
        <div class="row">
            <div class="col-12">
                <h1 class="fw-bold text-center">About Me</h1>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('images/prof.JPG') }}" alt="" height="500px" width="400px">
            </div>
           <div class="col-md-6">
               <h2 class="fw-400">JACKELYN CORPUZ</h2>
               <h4 class="text-warning">A R T L O V E R</h4>
               <p class="lead mt-2";>
                    Hello, my name is Jackelyn Corpuz from San Manuel, Pangasinan. I'm 23 years old and I am currently studying at Pangasinan State University - Urdaneta City Campus taking a BS in Information Technology course.
                    During my whole life and experience, I have been interested in Art and Creativity. My favorite thing about creating art is that it completely absorbs me mentally. Whenever I’m overwhelmed or stressed out, I can just start doodling or drawing or painting, and it’ll only be a few minutes before I completely forget about all my troubles. I can literally spend hours at a time like that, just focusing on creating something that is my own and that I have complete control over. It’s highly engaging, and yet also relaxing and fun. I’ve just always loved it.
                </p>
                <button type="button" class="btn btn-warning text-black px-5 py-2" data-bs-toggle="modal" data-bs-target="#exampleModal">
                My Resume
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">My Digital Resume</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <img src="{{ asset('images/resume.PNG') }}" height="1060px" class="border border-3">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-bs-dismiss="modal">Close</button>
                    </div>
                    </div>
                </div>
                </div>
               
           </div> 
        </div>
    </div>
</section>

<section id="gallery">
    <div class="container my-4 py-4">
            <div class="row mb-5">
                <div class="col-12">
                    <h1 class="fw-bold text-center">Gallery</h1>
                    <hr>
                </div>
                <div class="col-md-4">
                <div class="card p-4" style="width: 19rem;">
                    <img src="{{ asset('images/1.JPG') }}" class="card-img-top" alt="...">
                    <div class="card-body text-center">
                        <h4 class="text-success">Philippians 4:13</h4> 
                        <p class="card-text">"I can do all things through Christ who strengthens me."</p>
                    </div>
                    </div>
                </div>
                <div class="col-md-4">
                <div class="card p-4" style="width: 19rem;">
                    <img src="{{ asset('images/2.JPG') }}" class="card-img-top" alt="...">
                    <div class="card-body text-center">
                    <h4 class="text-warning">Matthew 19:26</h4>
                        <p class="card-text">"But Jesus looked at them and said, With man this is impossible, but with God all things are possible."</p>
                    </div>
                    </div>
                </div>
                <div class="col-md-4">
                <div class="card p-4" style="width: 19rem;">
                    <img src="{{ asset('images/3.JPG') }}" class="card-img-top" alt="...">
                    <div class="card-body text-center">
                        <h4 class="text-primary">Jeremiah 29:11</h4>
                        <p class="card-text">"For I know the plans I have for you, declares the Lord, plans to prosper you and not to harm you, plans to give you hope and a future."</p>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>      
</section>

<section id="contact">
    <div class="container my-5 py-5">
        <div class="row mb-5">
            <div class="col-12">
                <h1 class="fw-bold text-center">Contact Us</h1>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 mx-auto">
                <form action="">
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" 
                        class="form-label">Full Name</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Maria Del Bario" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Email Address</label>
                        <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Your Message</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" placeholder="Type your message..." required></textarea>
                    </div>
                    <button type="submit" class="btn btn-warning">Send Message</button>
                </form>
            </div>
        </div>
    </div>      
</section>

<section id="contact">
    <div class="container my-5 py-5">
        <div class="row mb-5">
            <div class="col-12">
                <h1 class="fw-bold text-center">Login</h1>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 mx-auto">
                <form action="">
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Email Address</label>
                        <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Password</label>
                        <input type="password" id="exampleFormControlTextarea1" required>
                    </div>
                    <button type="submit" class="btn btn-warning">Login</button>
                </form>
            </div>
        </div>
    </div>      
</section>

<section id="contact">
    <div class="container my-5 py-5">
        <div class="row mb-5">
            <div class="col-12">
                <h1 class="fw-bold text-center">Registration</h1>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 mx-auto">
                <form action="">
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" 
                        class="form-label">Full Name</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Email Address</label>
                        <input type="email" class="form-control" id="exampleFormControlInput1" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" 
                        class="form-label">Password</label>
                        <input type="password" class="form-control" id="exampleFormControlInput1" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" 
                        class="form-label">Confirm Password</label>
                        <input type="password" class="form-control" id="exampleFormControlInput1" required>
                    </div>
                    <button type="submit" class="btn btn-warning">Submit</button>
                </form>
            </div>
        </div>
    </div>      
</section>

@endsection
