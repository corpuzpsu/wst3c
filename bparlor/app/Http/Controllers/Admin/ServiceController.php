<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServiceStoreRequest;
use App\Models\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Services::all();
        return view('admin.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceStoreRequest $request)
    {
        $image = $request->file('image')->store('public/services');

        Services::create([
            'id' => $request->id,
            'image' => $image,
            'service_name' => $request->service_name,
            'price' =>  $request->price
        ]);

        return to_route('admin.services.index')->with('success', 'New service created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Services $service)
    {
        return view('admin.services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceStoreRequest $request, Services $service)
    {
        

        $request->validate([
            
            'service_name' => 'required',
            'price' => 'required'
        ]);

        $image = $service->image;
        if($request->hasFile('image')){
            Storage::delete($service->image);
            $image = $request->file('image')->store('public/services');
        }

        $service->update([
            
            'image' => $image,
            'service_name' => $request->service_name,
            'price' => $request->price
        ]);

        return to_route('admin.services.index')->with('success', 'Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Services $service)
    {
        Storage::delete($service->image);
        $service->delete();

        return to_route('admin.services.index')->with('danger', 'Deleted successfully.');
    }
}
