<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Copyright;
use Illuminate\Http\Request;

class CopyrightController extends Controller
{
    public function index()
    {
        $copyrights = Copyright::all();
        return view('copyrights.index', compact('copyrights'));
    }
}
