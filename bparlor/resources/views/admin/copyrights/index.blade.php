<x-admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
<div class="relative overflow-x-auto shadow-md sm:rounded-lg">
    <table class="w-full text-sm text-left text-gray-500 dark:bg-slate-400">
        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="px-4 py-4">
                    Copyright Year
                </th>
               
                <th scope="col" class="px-4 py-4">
                    Action
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($copyrights as $copyright)
            <tr class="bg-white text-gray-700 tracking-wide border-b dark:bg-gray-800 dark:border-gray-700">
                <td class="font-normal px-4 py-6">
                    {{ $copyright->year }}
                </td>
    
                <td class="px-4 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap dark:text-white">
                    <div class="flex space-x-2">
                        <a href="{{ route('admin.copyrights.edit', $copyright->id) }}" 
                            class="px-4 py-2 bg-slate-500 hover:bg-slate-700 rounded-lg text-white">Edit</a>
                    </div>
                    
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    
    </div>
    </div>
</x-admin-layout>
