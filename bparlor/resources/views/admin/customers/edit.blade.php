<x-admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">   
            <div class="flex m-2 p-2">
                <a href="{{ route('admin.customers.index') }}" 
                class="px-4 py-2 bg-slate-500 hover:bg-slate-700 rounded-lg text-white">Customer Index</a>
            </div>
            <div class="m-2 p-2 bg-slate-100 rounded">
                <div class="space-y-8 divide-y divide-gray-200 w-1/2 mt-10">
                    <form method="POST" action="{{ route('admin.customers.update', $customer->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="sm:col-span-6">
                            <label for="name" class="block text-sm font-medium text-gray-700"> Name </label>
                                <div class="mt-1">
                                    <input type="text" id="name" name="name" value="{{ $customer->name }}"
                                        class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('name') border-red-400 @enderror" />
                                </div>
                                @error('name')
                                <div class="text-sm text-red-400">{{ $message }}</div>
                            @enderror
                            
                            <label for="email" class="block text-sm font-medium text-gray-700"> Email </label>
                                <div class="mt-1">
                                    <input type="email" id="email" name="email" value="{{ $customer->email }}"
                                        class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('email') border-red-400 @enderror" />
                                </div>
                                @error('email')
                                <div class="text-sm text-red-400">{{ $message }}</div>
                                @enderror
                            
                            <label for="phone" class="block text-sm font-medium text-gray-700"> Phone Number </label>
                                <div class="mt-1">
                                    <input type="text" id="phone" name="phone" value="{{ $customer->phone }}"
                                        class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('phone') border-red-400 @enderror" />
                                </div>
                                @error('phone')
                                <div class="text-sm text-red-400">{{ $message }}</div>
                                @enderror

                            <label for="gender" class="block text-sm font-medium text-gray-700"> Gender </label>
                                <div class="mt-1">
                                    <input type="text" id="gender" name="gender" value="{{ $customer->gender }}"
                                        class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('gender') border-red-400 @enderror" />
                                </div>
                                @error('gender')
                                <div class="text-sm text-red-400">{{ $message }}</div>
                                @enderror

                            <label for="details" class="block text-sm font-medium text-gray-700"> Details </label>
                                <div class="mt-1">
                                    <textarea rows="3" id="details" name="details"
                                        class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('details') border-red-400 @enderror" 
                                    >{{ $customer->details }}</textarea>
                                </div>
                                @error('details')
                                <div class="text-sm text-red-400">{{ $message }}</div>
                                @enderror
                        
                        <div class="mt-5 p-2">
                            <button type="submit" 
                                class="px-4 py-2 bg-blue-500 hover:bg-blue-600 rounded-lg text-white">Update</button>
                        </div>
                    </form>
                </div>

            </div>
            
        </div>  
    </div>
</x-admin-layout>
