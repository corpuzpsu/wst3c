<x-admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class=" flex justify-end m-2 p-2">
                <a href="{{ route('admin.galleries.create') }}" 
                class="px-4 py-2 bg-green-500 hover:bg-green-700 rounded-lg text-white">+ New Image</a>
            </div>
            
<div class="relative overflow-x-auto shadow-md sm:rounded-lg">
    <table class="w-full text-sm text-left text-gray-500 dark:bg-slate-400">
        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="px-2 py-3">
                    No.
                </th>
                <th scope="col" class="px-2 py-3">
                    Service Name
                </th>
                <th scope="col" class="px-2 py-3">
                    Image
                </th>
                <th scope="col" class="px-2 py-3">
                    Action
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($galleries as $gallery)
            <tr class="bg-white text-gray-700 tracking-wide border-b dark:bg-gray-800 dark:border-gray-700">
                <td class="font-semibold px-2 py-4">
                    {{ $gallery->id }}
                </td>
                <td class="font-semibold px-2 py-4">
                    {{ $gallery->name }}
                </td>
                <td class="font-semibold px-2 py-4">
                    <img src="{{ Storage::url($gallery->image) }}" class="w-16 h-16 rounded">
                </td>
    
                <td class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap dark:text-white">
                    <div class="flex space-x-2">
                        <a href="{{ route('admin.galleries.edit', $gallery->id) }}" 
                            class="px-4 py-2 bg-slate-500 hover:bg-slate-700 rounded-lg text-white">Edit</a>
                            <form class="px-4 py-2 bg-red-500 hover:bg-red-700 rounded-lg text-white" 
                            method="POST" action="{{ route('admin.galleries.destroy', $gallery->id) }}"
                            onsubmit="return confirm('Are you sure?');">
                            @csrf
                            @method('DELETE')
                            <button type="submit">Delete</button>
                        </form>
                    </div>
                    
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    
    </div>
    </div>
</x-admin-layout>
