<x-admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">   
            <div class="flex m-2 p-2">
                <a href="{{ route('admin.menus.index') }}" 
                class="px-4 py-2 bg-slate-500 hover:bg-slate-700 rounded-lg text-white">Service index</a>
            </div>
            <div class="m-2 p-2 bg-slate-100 rounded">
                <div class="space-y-8 divide-y divide-gray-200 w-1/2 mt-10">
                    <form method="POST" action="{{ route('admin.menus.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="sm:col-span-6">
                        <label for="name" class="block text-sm font-medium text-gray-700"> Service Name </label>
                            <div class="mt-1">
                                <input type="text" id="name" name="name" 
                                    class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                            </div>
                        </div>
                        <div class="sm:col-span-6 pt-5">
                            <label for="description" class="block text-sm font-medium text-gray-700">Description</label>
                            <div class="mt-1">
                                <textarea id="description" name="description" rows="3" class="shadow-sm focus:ring-indigo-500 appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"></textarea>
                            </div>
                            </div>
                        <div class="sm:col-span-6">
                       
                        <div class="sm:col-span-6">
                            <label for="price" class="block text-sm font-medium text-gray-700"> Price </label>
                            <div class="mt-1">
                                <input type="number" min="0.00" max="10000.00" id="price" name="price" 
                                    class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                            </div>
                        </div>
                        
                        <div class="sm:col-span-6 pt-5">
                            <label for="body" class="block text-sm font-medium text-gray-700">Categories</label>
                            <div class="mt-1">
                               <select id="categories" name="categories[]" class="form-multiselect block w-full mt-1" multiple>
                                @foreach ($categories as $category)
                                    <option>{{ $category->name }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="mt-5 p-2">
                            <button type="submit" 
                                class="px-4 py-2 bg-slate-500 hover:bg-slate-700 rounded-lg text-white">ADD</button>
                        </div>
                    </form>
                </div>
            </div>  
        </div>  
    </div> <br>
    <footer class="bg-white border-gray-200 py-2 mt-10">
    
            <p class="select-none -mb-10 flex flex-wrap items-center justify-center text-gray-700 text-center text-lg tracking-wide">
              &copy; 2022 Chanelle Beauty Parlor
            </p>
      
    </footer>
</x-admin-layout>
