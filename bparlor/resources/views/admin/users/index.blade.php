<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'ChanelleBeauty') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body>
        <div class="sticky top-0 bg-white shadow-md" x-data="{ isOpen: false }">
            <nav class="container px-6 py-3 mx:auto md:flex md:justify-between md:items-center">
              <div class="flex items-center justify-between">
                <a class="text-xl font-bold text-transparent bg-clip-text bg-gradient-to-r from-pink-600 to-gray-700 md:text-2xl hover:text-pink-400 border-solid border-2 border-b-pink-600 border-r-pink-600 border-l-gray-600 border-t-gray-600"
                  href="#">
                  ChanelleBeauty.     
                </a> 
                <!-- Mobile menu button -->
                <div @click ="isOpen = !isOpen" class="flex md:hidden">
                  <button type="button" class="text-gray-800 hover:text-gray-400 focus:outline-none focus:text-gray-400"
                    aria-label="toggle menu">
                    <svg viewBox="0 0 24 24" class="w-6 h-6 fill-current">
                      <path fill-rule="evenodd"
                        d="M4 5h16a1 1 0 0 1 0 2H4a1 1 0 1 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2z">
                      </path>
                    </svg>
                  </button>
                </div>
              </div>
      
              <!-- Mobile Menu open: "block", Menu closed: "hidden" -->
              <div :class="isOpen ? 'flex' : 'hidden'"
                class="flex-col mt-8 space-y-4 md:flex md:space-y-0 md:flex-row md:items-center md:space-x-10 md:mt-0">
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="/">Home</a>
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="{{ route('about.index') }}">About</a>
                <a class="text-gray-800 font-semibold bg-clip-text text-pink-600 underline"
                  href="{{ route('services.index') }}">Services</a>
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="{{ route('gallery.index') }}">Gallery</a>
                <a class="text-gray-800 font-semibold bg-clip-text  bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="{{ route('reservations.step.one') }}">Appointment</a>
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="{{ route('contact.index') }}">Contact</a>
              </div>
            </nav>
          </div>

          <section id="services" class="bg-fixed bg-cover py-20" 
          style="background-image: url('https://img.freepik.com/free-vector/pink-watercolor-leaves-background_23-2148907681.jpg?w=2000')">
            <div class="tracking-wide text-center">
           
              <h1 class="tracking-wide text-4xl font-bold text-gray-800 bg-clip-text bg-gradient-to-r from-green-400 to-blue-500">
                 OUR SERVICE PRICES</h1>
            </div>
            <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="relative overflow-x-auto shadow-lg sm:rounded-lg">
                <table class="table-auto w-full to border-collapse border border-gray-400 text-medium text-center text-gray-700 dark:bg-slate-400">
                    <thead class="text-medium text-gray-700 uppercase bg-pink-100 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="font-bold border border-gray-400 py-3">
                                Service #
                            </th>
                            <th scope="col" class="border border-gray-400 py-3">
                                Service Name
                            </th>
                            <th scope="col" class="border border-gray-400 py-3">
                                Service Price
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($services as $service)
                        <tr class="font-semibold bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                            <td class="font-extrabold border border-gray-400 py-4">
                                {{ $service->id }}
                            </td>
                            <td class="border border-gray-400 py-4">
                                {{ $service->service_name }}
                            </td>
                            <td class="border border-gray-400 py-4">
                                {{ $service->price }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            </div>
          </section>

          <section class="pt-4 pb-12 bg-gray-800">
            <div class="my-16 text-center">
              <h2 class="text-5xl font-bold tracking-widest text-white bg-clip-text bg-gradient-to-r from-green-400 to-blue-500">
                CLIENTS TESTIMONIALS </h2>
              <p class="text-2xl text-white italic">Happy clients about us.</p>
            </div>
            <br>
            <div class="grid gap-2 lg:grid-cols-3">
              <div class="max-w-md p-4 bg-white rounded-lg shadow-lg">
                <div class="flex justify-center -mt-16 md:justify-end">
                  <img class="object-cover animate-bounce w-20 h-20 border-2 border-pink-500 rounded-full"
                    src="https://images.unsplash.com/photo-1499714608240-22fc6ad53fb2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80">
                </div>
                <div>
                  <p class="mt-2 text-gray-600">"Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae dolores deserunt
                    ea doloremque natus error, rerum quas odio quaerat nam ex commodi hic, suscipit in a veritatis pariatur
                    minus consequuntur!"</p>
                </div>
                <div class="flex justify-end mt-4">
                  <p href="#" class="text-xl font-medium text-pink-600 italic">- John Domak</p>
                </div>
              </div>
              <div class="max-w-md p-4 bg-white rounded-lg shadow-lg">
                <div class="flex justify-center -mt-16 md:justify-end">
                  <img class="object-cover animate-bounce w-20 h-20 border-2 border-pink-500 rounded-full"
                    src="https://cdn.pixabay.com/photo/2018/01/04/21/15/young-3061652__340.jpg">
                </div>
                <div>
                  <p class="mt-2 text-gray-600">"Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae dolores deserunt
                    ea doloremque natus error, rerum quas odio quaerat nam ex commodi hic, suscipit in a veritatis pariatur
                    minus consequuntur!"</p>
                </div>
                <div class="flex justify-end mt-4">
                  <a href="#" class="text-xl font-medium text-pink-600 italic">- Maria del Barrio</a>
                </div>
              </div>
              <div class="max-w-md p-4 bg-white rounded-lg shadow-lg">
                <div class="flex justify-center -mt-16 md:justify-end">
                  <img class="object-cover animate-bounce w-20 h-20 border-2 border-pink-500 rounded-full"
                    src="https://cdn.pixabay.com/photo/2018/01/18/17/48/purchase-3090818__340.jpg">
                </div>
                <div>
                  <p class="mt-2 text-gray-600">"Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae dolores deserunt
                    ea doloremque natus error, rerum quas odio quaerat nam ex commodi hic, suscipit in a veritatis pariatur
                    minus consequuntur!"</p>
                </div>
                <div class="flex justify-end mt-4">
                  <a href="#" class="text-xl font-medium text-pink-600 italic">- Bella Corpuz</a>
                </div>
                <br>
              </div>
            </div>
          </section>

        <section id="contact" class="bg-cover bg-fixed py-20" style="background-image: url('https://img.freepik.com/free-vector/pink-watercolor-leaves-background_23-2148907681.jpg?w=2000')">
        <div class="container items-center mx-auto">      
        <div class="flex flex-wrap items-center -mx-3">
            <div class="gap-2 columns-1 order-1 w-full px-3 lg:w-1/2 lg:order-0">
            <div class="w-full lg:max-w-md ml-14 my-12 bg-pink-100 max-w-xs mx-4 mb-2 mt-4 rounded-r-full shadow-lg">
                <h2
                class="text-4xl tracking-widest font-extrabold bg-clip-text text-pink-600 mb-6">
                CONTACT US</h2>
                
                <p class="text-xl tracking-widest text-gray-800 mb-10">HAVE A QUESTION?</p>
            
                <ul>
                <li class="flex items-center py-2 space-x-4 xl:py-3">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="w-8 h-8 text-white" fill="#DB7093">
                    <path d="M408 120C408 174.6 334.9 271.9 302.8 311.1C295.1 321.6 280.9 321.6 273.2 311.1C241.1 271.9 168 174.6 168 120C168 53.73 221.7 0 288 0C354.3 0 408 53.73 408 120zM288 152C310.1 152 328 134.1 328 112C328 89.91 310.1 72 288 72C265.9 72 248 89.91 248 112C248 134.1 265.9 152 288 152zM425.6 179.8C426.1 178.6 426.6 177.4 427.1 176.1L543.1 129.7C558.9 123.4 576 135 576 152V422.8C576 432.6 570 441.4 560.9 445.1L416 503V200.4C419.5 193.5 422.7 186.7 425.6 179.8zM150.4 179.8C153.3 186.7 156.5 193.5 160 200.4V451.8L32.91 502.7C17.15 508.1 0 497.4 0 480.4V209.6C0 199.8 5.975 190.1 15.09 187.3L137.6 138.3C140 152.5 144.9 166.6 150.4 179.8H150.4zM327.8 331.1C341.7 314.6 363.5 286.3 384 255V504.3L192 449.4V255C212.5 286.3 234.3 314.6 248.2 331.1C268.7 357.6 307.3 357.6 327.8 331.1L327.8 331.1z"/>
                    </svg>
                    <p class="text-base font-medium tracking-wide text-gray-800">
                    Location: Asingan, 2438 Pangasinan, Philippines
                    </p>
                </li>    

                <li class="flex items-center py-2 space-x-4 xl:py-3">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="w-8 h-8 text-white" fill="#DB7093">
                    <path d="M511.2 387l-23.25 100.8c-3.266 14.25-15.79 24.22-30.46 24.22C205.2 512 0 306.8 0 54.5c0-14.66 9.969-27.2 24.22-30.45l100.8-23.25C139.7-2.602 154.7 5.018 160.8 18.92l46.52 108.5c5.438 12.78 1.77 27.67-8.98 36.45L144.5 207.1c33.98 69.22 90.26 125.5 159.5 159.5l44.08-53.8c8.688-10.78 23.69-14.51 36.47-8.975l108.5 46.51C506.1 357.2 514.6 372.4 511.2 387z"/>
                    </svg>
                    <p class="text-base font-medium tracking-wide text-gray-800">
                    Phone: +639 985 1234 789
                    </p>
                </li>  
                
                <li class="flex items-center py-2 space-x-4 xl:py-3">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="w-8 h-8 text-white" fill="#DB7093">
                    <path d="M256 417.1c-16.38 0-32.88-4.1-46.88-15.12L0 250.9v213.1C0 490.5 21.5 512 48 512h416c26.5 0 48-21.5 48-47.1V250.9l-209.1 151.1C288.9 412 272.4 417.1 256 417.1zM493.6 163C484.8 156 476.4 149.5 464 140.1v-44.12c0-26.5-21.5-48-48-48l-77.5 .0016c-3.125-2.25-5.875-4.25-9.125-6.5C312.6 29.13 279.3-.3732 256 .0018C232.8-.3732 199.4 29.13 182.6 41.5c-3.25 2.25-6 4.25-9.125 6.5L96 48c-26.5 0-48 21.5-48 48v44.12C35.63 149.5 27.25 156 18.38 163C6.75 172 0 186 0 200.8v10.62l96 69.37V96h320v184.7l96-69.37V200.8C512 186 505.3 172 493.6 163zM176 255.1h160c8.836 0 16-7.164 16-15.1c0-8.838-7.164-16-16-16h-160c-8.836 0-16 7.162-16 16C160 248.8 167.2 255.1 176 255.1zM176 191.1h160c8.836 0 16-7.164 16-16c0-8.838-7.164-15.1-16-15.1h-160c-8.836 0-16 7.162-16 15.1C160 184.8 167.2 191.1 176 191.1z"/>
                    </svg>
                    <p class="select-none text-base font-medium tracking-wide text-gray-800">
                    Email: <a class="text-pink-700 underline decoration-pink-700 hover:text-gray-700">chanellebeauty@gmail.com</a>
                    </p>
                </li>   

                <li class="flex items-center py-2 space-x-4 xl:py-3">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="w-8 h-8 text-white" fill="#DB7093">
                    <path d="M352 256C352 278.2 350.8 299.6 348.7 320H163.3C161.2 299.6 159.1 278.2 159.1 256C159.1 233.8 161.2 212.4 163.3 192H348.7C350.8 212.4 352 233.8 352 256zM503.9 192C509.2 212.5 512 233.9 512 256C512 278.1 509.2 299.5 503.9 320H380.8C382.9 299.4 384 277.1 384 256C384 234 382.9 212.6 380.8 192H503.9zM493.4 160H376.7C366.7 96.14 346.9 42.62 321.4 8.442C399.8 29.09 463.4 85.94 493.4 160zM344.3 160H167.7C173.8 123.6 183.2 91.38 194.7 65.35C205.2 41.74 216.9 24.61 228.2 13.81C239.4 3.178 248.7 0 256 0C263.3 0 272.6 3.178 283.8 13.81C295.1 24.61 306.8 41.74 317.3 65.35C328.8 91.38 338.2 123.6 344.3 160H344.3zM18.61 160C48.59 85.94 112.2 29.09 190.6 8.442C165.1 42.62 145.3 96.14 135.3 160H18.61zM131.2 192C129.1 212.6 127.1 234 127.1 256C127.1 277.1 129.1 299.4 131.2 320H8.065C2.8 299.5 0 278.1 0 256C0 233.9 2.8 212.5 8.065 192H131.2zM194.7 446.6C183.2 420.6 173.8 388.4 167.7 352H344.3C338.2 388.4 328.8 420.6 317.3 446.6C306.8 470.3 295.1 487.4 283.8 498.2C272.6 508.8 263.3 512 255.1 512C248.7 512 239.4 508.8 228.2 498.2C216.9 487.4 205.2 470.3 194.7 446.6H194.7zM190.6 503.6C112.2 482.9 48.59 426.1 18.61 352H135.3C145.3 415.9 165.1 469.4 190.6 503.6V503.6zM321.4 503.6C346.9 469.4 366.7 415.9 376.7 352H493.4C463.4 426.1 399.8 482.9 321.4 503.6V503.6z"/>
                    </svg>
                    <p class="select-none text-base font-medium tracking-wide">
                    Website: <a class="text-pink-700 underline decoration-pink-700 hover:text-gray-700">www.chanellebeauty.com</a>
                    </p> <br>
                </li>   
                
                </ul>            
            </div>         
            </div>
        <div class="w-full mx-auto px-2 lg:w-1/2 order-0 lg:order-1 lg:mb-0 py-20 bg-cover "
        style="background-image: url('https://map.viamichelin.com/map/carte?map=viamichelin&z=10&lat=16.0035&lon=120.66861&width=550&height=382&format=png&version=latest&layer=background&debug_pattern=.*')">     
        <div class="my-8 text-center">
                <p class="text-xl text-transparent">VISIT US ON SOCIAL NETWORKS</p>
                <h2 class="text-5xl font-bold text-transparent">
                .</h2> 
            </div>
            <br><br><br><br>
            </div>
        </div>
        </div>
        </section>

        <footer class="bg-gray-800 border-t border-gray-200 py-12">
        <div class="w-full my-8 text-center">
        <p class="text-xl tracking-widest text-white mb-2">VISIT US ON SOCIAL NETWORKS</p>
        <h2 class="text-4xl font-extrabold tracking-widest text-white mb-20">
          FOLLOW US</h2> 
         </div>          
          <div class="flex justify-center mt-4 lg:mt-0 space-x-5">
            <a>
              <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                class="animate-bounce w-14 h-14 text-white bg-pink-600 rounded-full hover:bg-blue-700 hover:text-white" viewBox="0 0 24 24">
                <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
              </svg>
            </a>
            <a class="ml-3">
              <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                class="animate-bounce w-14 h-14 text-white bg-pink-600 rounded-full hover:bg-blue-400 hover:text-white items-center" viewBox="0 0 26 30">
                <path
                  d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z">
                </path>
              </svg>
            </a>
            <a class="ml-3">
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                class="animate-bounce w-14 h-14 text-white bg-pink-600 rounded-full hover:bg-gradient-to-r from-violet-700 via-red-400 to-red-500 hover:text-white" viewBox="0 0 24 26">
                <rect width="18" height="16" x="3" y="5" rx="3" ry="5"></rect>
                <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
              </svg>
            </a>
            <a class="ml-3">
              <svg fill="currentColor" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="0" 
                class="animate-bounce w-14 h-14 text-base text-white bg-pink-600 rounded-full hover:bg-blue-500 hover:text-white" viewBox="0 0 24 30">
                <path stroke="none"
                  d="M16 8a6 6 0 016 6v7h-4v-7a2 2 0 00-2-2 2 2 0 00-2 2v7h-4v-7a6 6 0 016-6zM2 9h4v12H2z"></path>
                <circle cx="4" cy="4" r="2" stroke="none"></circle>
              </svg>
            </a>
          </div>
          <div class="flex flex-wrap items-center justify-center px-4 py-8 mx-auto mt-8">
            <p class="select-none text-white text-center text-lg tracking-wide">
              &copy; 2022 <a class="text-lg text-pink-400 tracking-wide"> Chanelle Beauty Parlor </a>
            </p>
        </div>
      </footer>
</body>
</html>
