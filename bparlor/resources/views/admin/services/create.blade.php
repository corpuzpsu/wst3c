<x-admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">   
            <div class="flex m-2 p-2">
                <a href="{{ route('admin.services.index') }}" 
                class="px-4 py-2 bg-slate-500 hover:bg-slate-700 rounded-lg text-white">Services Index</a>
            </div>
            <div class="m-2 p-2 bg-slate-100 rounded">
                <div class="space-y-8 divide-y divide-gray-200 w-1/2 mt-10">
                    <form method="POST" action="{{ route('admin.services.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="sm:col-span-6">

                            <label for="image" class="block text-sm font-medium text-gray-700"> Image </label>
                            <div class="mt-1">
                                <input type="file" id="image" name="image" 
                                    class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5
                                    @error('image') border-red-400 @enderror" />
                            </div>
                            @error('image')
                            <div class="text-sm text-red-400">{{ $message }}</div>
                            @enderror

                        <label for="service_name" class="block text-sm font-medium text-gray-700"> Service Name </label>
                            <div class="mt-1">
                                <input type="text" id="service_name" name="service_name" 
                                    class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('service_name') border-red-400 @enderror" />
                            </div>
                            @error('service_name')
                            <div class="text-sm text-red-400">{{ $message }}</div>
                            @enderror
                        
                        <label for="price" class="block text-sm font-medium text-gray-700"> Price </label>
                        <div class="mt-1">
                            <input type="number" id="price" name="price" 
                                class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('price') border-red-400 @enderror" />
                        </div>
                        @error('price')
                        <div class="text-sm text-red-400">{{ $message }}</div>
                        @enderror
                    
                        </div>

                        <div class="mt-5 p-2">
                            <button type="submit" 
                                class="px-4 py-2 bg-blue-500 hover:bg-blue-500 rounded-lg text-white">Add Service</button>
                        </div>
                    </form>
                </div>

            </div>
            
        </div>  
    </div>
</x-admin-layout>
