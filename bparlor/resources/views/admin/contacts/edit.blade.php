<x-admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">   
            <div class="flex m-2 p-2">
                <a href="{{ route('admin.contacts.index') }}" 
                class="px-4 py-2 bg-slate-500 hover:bg-slate-700 rounded-lg text-white">Contact Index</a>
            </div>
            <div class="m-2 p-2 bg-slate-100 rounded">
                <div class="space-y-8 divide-y divide-gray-200 w-1/2 mt-10">
                    <form method="POST" action="{{ route('admin.contacts.update', $contact->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="sm:col-span-6">

                            <label for="location" class="block text-sm font-medium text-gray-700"> Location </label>
                                <div class="mt-1">
                                    <input type="text" id="location" name="location" value="{{ $contact->location }}"
                                        class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5 
                                        @error('location') border-red-400 @enderror" />
                                </div>
                                @error('location')
                                <div class="text-sm text-red-400">{{ $message }}</div>
                                @enderror

                            <label for="phone" class="block text-sm font-medium text-gray-700"> Phone Number </label>
                                <div class="mt-1">
                                    <input type="text" id="phone" name="phone" value="{{ $contact->phone }}"
                                        class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5 
                                        @error('phone') border-red-400 @enderror" />
                                </div>
                                @error('phone')
                                <div class="text-sm text-red-400">{{ $message }}</div>
                                @enderror
                            
                            <label for="email" class="block text-sm font-medium text-gray-700"> Email </label>
                                <div class="mt-1">
                                    <input type="email" id="email" name="email" value="{{ $contact->email }}"
                                        class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5 
                                        @error('email') border-red-400 @enderror" />
                                </div>
                                @error('email')
                                <div class="text-sm text-red-400">{{ $message }}</div>
                                @enderror
                        
                    
                        
                        <div class="mt-5 p-2">
                            <button type="submit" 
                                class="px-4 py-2 bg-blue-500 hover:bg-blue-600 rounded-lg text-white">Update</button>
                        </div>
                    </form>
                </div>

            </div>
            
        </div>  
    </div>
</x-admin-layout>
