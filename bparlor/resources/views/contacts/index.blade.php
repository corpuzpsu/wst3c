<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'ChanelleBeauty') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body>
        <div class="sticky top-0 bg-white shadow-md" x-data="{ isOpen: false }">
            <nav class="container px-6 py-3 mx:auto md:flex md:justify-between md:items-center">
              <div class="flex items-center justify-between">
                <a class="text-xl font-bold text-transparent bg-clip-text bg-gradient-to-r from-pink-600 to-gray-700 md:text-2xl hover:text-pink-400 border-solid border-2 border-b-pink-600 border-r-pink-600 border-l-gray-600 border-t-gray-600"
                  href="#">
                  ChanelleBeauty.     
                </a> 
                <!-- Mobile menu button -->
                <div @click ="isOpen = !isOpen" class="flex md:hidden">
                  <button type="button" class="text-gray-800 hover:text-gray-400 focus:outline-none focus:text-gray-400"
                    aria-label="toggle menu">
                    <svg viewBox="0 0 24 24" class="w-6 h-6 fill-current">
                      <path fill-rule="evenodd"
                        d="M4 5h16a1 1 0 0 1 0 2H4a1 1 0 1 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2z">
                      </path>
                    </svg>
                  </button>
                </div>
              </div>
      
              <!-- Mobile Menu open: "block", Menu closed: "hidden" -->
              <div :class="isOpen ? 'flex' : 'hidden'"
                class="flex-col mt-8 space-y-4 md:flex md:space-y-0 md:flex-row md:items-center md:space-x-10 md:mt-0">
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="/">Home</a>
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="{{ route('abouts.index') }}">About</a>
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="{{ route('services.index') }}">Services</a>
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="{{ route('galleries.index') }}">Gallery</a>
                <a class="text-gray-800 font-semibold bg-clip-text  bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="{{ route('reservations.step.one') }}">Appointment</a>
                <a class="text-gray-800 font-semibold bg-clip-text text-pink-600 underline"
                  href="{{ route('contacts.index') }}">Contact</a>
              </div>
            </nav>
          </div>


          <section class="bg-fixed bg-cover py-20" 
          style="background-image: url('https://img.freepik.com/free-vector/pink-watercolor-leaves-background_23-2148907681.jpg?w=2000')">
          <div class="container items-center max-w-6xl px-4 px-10 mx-auto sm:px-20 md:px-32 lg:px-16">      
            <div class="flex flex-wrap items-center -mx-3">
              <div class="order-1 w-full px-3 lg:w-1/2 lg:order-0">
                <div class="w-full lg:max-w-md">
                  <h2
                    class="mb-4 text-4xl tracking-wider font-extrabold bg-clip-text text-pink-600">
                    CONTACT US</h2>
                  
                    <p class="text-xl tracking-widest text-gray-800 mb-10">HAVE A QUESTION?</p>
                  <ul>
                    @foreach ($contacts as $contact)
                    <li class="flex items-center py-2 space-x-4 xl:py-3 bg-pink-100 rounded-r-full shadow-lg">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="w-16 h-16 text-white" fill="#DB7093">
                        <path d="M408 120C408 174.6 334.9 271.9 302.8 311.1C295.1 321.6 280.9 321.6 273.2 311.1C241.1 271.9 168 174.6 168 120C168 53.73 221.7 0 288 0C354.3 0 408 53.73 408 120zM288 152C310.1 152 328 134.1 328 112C328 89.91 310.1 72 288 72C265.9 72 248 89.91 248 112C248 134.1 265.9 152 288 152zM425.6 179.8C426.1 178.6 426.6 177.4 427.1 176.1L543.1 129.7C558.9 123.4 576 135 576 152V422.8C576 432.6 570 441.4 560.9 445.1L416 503V200.4C419.5 193.5 422.7 186.7 425.6 179.8zM150.4 179.8C153.3 186.7 156.5 193.5 160 200.4V451.8L32.91 502.7C17.15 508.1 0 497.4 0 480.4V209.6C0 199.8 5.975 190.1 15.09 187.3L137.6 138.3C140 152.5 144.9 166.6 150.4 179.8H150.4zM327.8 331.1C341.7 314.6 363.5 286.3 384 255V504.3L192 449.4V255C212.5 286.3 234.3 314.6 248.2 331.1C268.7 357.6 307.3 357.6 327.8 331.1L327.8 331.1z"/>
                      </svg>
                      <span class="text-lg font-extrabold tracking-wide text-gray-800">Location: 
                        <h1 class="text-lg font-semibold text-gray-800">{{ $contact->location }}</h1>
                      </span>
                
                    </li>
    
                   <li class="flex items-center py-2 space-x-4 xl:py-3 mt-2 bg-pink-50 rounded-r-full shadow-lg">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="w-16 h-16 text-white" fill="#DB7093">
                      <path d="M511.2 387l-23.25 100.8c-3.266 14.25-15.79 24.22-30.46 24.22C205.2 512 0 306.8 0 54.5c0-14.66 9.969-27.2 24.22-30.45l100.8-23.25C139.7-2.602 154.7 5.018 160.8 18.92l46.52 108.5c5.438 12.78 1.77 27.67-8.98 36.45L144.5 207.1c33.98 69.22 90.26 125.5 159.5 159.5l44.08-53.8c8.688-10.78 23.69-14.51 36.47-8.975l108.5 46.51C506.1 357.2 514.6 372.4 511.2 387z"/>
                    </svg>
                    <span class="text-lg font-extrabold tracking-wide text-gray-800">Phone: 
                      <h1 class="text-lg font-semibold text-gray-800">{{ $contact->phone }}</h1>
                    </span>
                    
                    </li>
    
                    <li class="flex items-center py-2 space-x-4 xl:py-3 mt-2 bg-pink-100 rounded-r-full shadow-lg">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="w-16 h-16 text-white" fill="#DB7093">
                        <path d="M256 417.1c-16.38 0-32.88-4.1-46.88-15.12L0 250.9v213.1C0 490.5 21.5 512 48 512h416c26.5 0 48-21.5 48-47.1V250.9l-209.1 151.1C288.9 412 272.4 417.1 256 417.1zM493.6 163C484.8 156 476.4 149.5 464 140.1v-44.12c0-26.5-21.5-48-48-48l-77.5 .0016c-3.125-2.25-5.875-4.25-9.125-6.5C312.6 29.13 279.3-.3732 256 .0018C232.8-.3732 199.4 29.13 182.6 41.5c-3.25 2.25-6 4.25-9.125 6.5L96 48c-26.5 0-48 21.5-48 48v44.12C35.63 149.5 27.25 156 18.38 163C6.75 172 0 186 0 200.8v10.62l96 69.37V96h320v184.7l96-69.37V200.8C512 186 505.3 172 493.6 163zM176 255.1h160c8.836 0 16-7.164 16-15.1c0-8.838-7.164-16-16-16h-160c-8.836 0-16 7.162-16 16C160 248.8 167.2 255.1 176 255.1zM176 191.1h160c8.836 0 16-7.164 16-16c0-8.838-7.164-15.1-16-15.1h-160c-8.836 0-16 7.162-16 15.1C160 184.8 167.2 191.1 176 191.1z"/>
                      </svg>
                      <span class="text-lg font-extrabold tracking-wide text-gray-800">Email: 
                        <h1 class="text-lg font-semibold text-pink-700 underline decoration-pink-700 hover:text-gray-700">{{ $contact->email }}</h1>
                        
                      </span>
                    
                    </li>                
                    @endforeach
                  </ul>
                </div>
              </div>
              <div class="w-full px-1 mb-8 lg:w-1/2 order-0 lg:order-1 lg:mb-0 py-20">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2780.579524512018!2d120.66801628359059!3d16.003023017747473!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x339117cd86326c4f%3A0x5d2a6e8d1f499a28!2sAsingan%20Public%20Market%2C%20Public%20Market%20Rd%2C%20Asingan%2C%202439%20Pangasinan!5e1!3m2!1sen!2sph!4v1655452296977!5m2!1sen!2sph" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade">
                </iframe>
                </div>
            </div>
          </div>
        </section>


        <footer class="bg-gray-800 border-t border-gray-200 py-12">
        <div class="w-full my-8 text-center">
        <p class="text-xl tracking-widest text-white mb-2">VISIT US ON SOCIAL NETWORKS</p>
        <h2 class="text-4xl font-extrabold tracking-widest text-white mb-20">
          FOLLOW US</h2> 
         </div>          
          <div class="flex justify-center mt-4 lg:mt-0 space-x-5">
            <a href="https://www.facebook.com/manilyn.nicolas.1" target="_blank">
              <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                class="animate-bounce w-14 h-14 text-white bg-pink-600 rounded-full hover:bg-blue-700 hover:text-white" viewBox="0 0 24 24">
                <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
              </svg>
            </a>
            <a class="ml-3">
              <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                class="animate-bounce w-14 h-14 text-white bg-pink-600 rounded-full hover:bg-blue-400 hover:text-white items-center" viewBox="0 0 26 30">
                <path
                  d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z">
                </path>
              </svg>
            </a>
            <a class="ml-3">
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                class="animate-bounce w-14 h-14 text-white bg-pink-600 rounded-full hover:bg-gradient-to-r from-violet-700 via-red-400 to-red-500 hover:text-white" viewBox="0 0 24 26">
                <rect width="18" height="16" x="3" y="5" rx="3" ry="5"></rect>
                <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
              </svg>
            </a>
           
          </div>
          <div class="flex flex-wrap items-center justify-center px-4 py-8 mx-auto mt-8">
            <p class="select-none text-white text-center text-lg tracking-wide">
              Copyright &copy; 2022 <a class="text-lg text-pink-400 tracking-wide"> Chanelle Beauty Parlor </a>
            </p>
        </div>
        <div class="flex flex-wrap items-center justify-center">
          <p class="select-none text-white text-center text-lg tracking-wide">
            Developed by Jackelyn Corpuz 
          </p>
      </div>
      </footer>
</body>
</html>
