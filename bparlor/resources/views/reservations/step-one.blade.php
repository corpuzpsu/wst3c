<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'ChanelleBeauty') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body>
        <div class="sticky top-0 bg-white shadow-md" x-data="{ isOpen: false }">
            <nav class="container px-6 py-3 mx:auto md:flex md:justify-between md:items-center">
              <div class="flex items-center justify-between">
                <a class="text-xl font-bold text-transparent bg-clip-text bg-gradient-to-r from-pink-600 to-gray-700 md:text-2xl hover:text-pink-400 border-solid border-2 border-b-pink-600 border-r-pink-600 border-l-gray-600 border-t-gray-600"
                  href="#">
                  ChanelleBeauty.     
                </a> 
                <!-- Mobile menu button -->
                <div @click ="isOpen = !isOpen" class="flex md:hidden">
                  <button type="button" class="text-gray-800 hover:text-gray-400 focus:outline-none focus:text-gray-400"
                    aria-label="toggle menu">
                    <svg viewBox="0 0 24 24" class="w-6 h-6 fill-current">
                      <path fill-rule="evenodd"
                        d="M4 5h16a1 1 0 0 1 0 2H4a1 1 0 1 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2z">
                      </path>
                    </svg>
                  </button>
                </div>
              </div>
      
              <!-- Mobile Menu open: "block", Menu closed: "hidden" -->
              <div :class="isOpen ? 'flex' : 'hidden'"
                class="flex-col mt-8 space-y-4 md:flex md:space-y-0 md:flex-row md:items-center md:space-x-10 md:mt-0">
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="/">Home</a>
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="{{ route('abouts.index') }}">About</a>
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="{{ route('services.index') }}">Services</a>
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="{{ route('galleries.index') }}">Gallery</a>
                <a class="text-gray-800 font-semibold bg-clip-text text-pink-600 underline hover:underline"
                  href="{{ route('reservations.step.one') }}">Appointment</a>
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="{{ route('contacts.index') }}">Contact</a>
              </div>
            </nav>
          </div>

    <div class="bg-cover py-20" 
        style="background-image: url('https://img5.goodfon.com/wallpaper/nbig/6/ad/tender-white-flowers-pink-background-rozovyi-fon-belye-tsvet.jpg')">
        <div class="container flex items-center justify-center p-6 mx-auto bg-white shadow-lg sm:p-12 md:w-1/2">

            <div class="w-full">
                <h3 class=" items-center justify-center text-center font-sans mb-6 text-2xl font-bold text-transparent bg-clip-text bg-gradient-to-r from-pink-600 to-gray-700">
                  <a class="text-pink-400 text-lg font-bold">MAKE AN</a> <br>APPOINTMENT/INQUIRY</h3>
                    
                <form method="POST" action="{{ route('reservations.store.step.one') }}">
                    @csrf
            
                    <div class="sm:col-span-5">
                        <label for="name" class="block text-sm font-medium text-gray-700"> Name
                        </label>
                        <div class="mt-1">
                            <input type="text" id="name" name="name"
                                value="{{ $reservation->name ?? '' }}"
                                class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                        </div>
                        @error('name')
                            <div class="text-sm text-red-400">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="sm:col-span-5">
                        <label for="email" class="block text-sm font-medium text-gray-700"> Email </label>
                        <div class="mt-1">
                            <input type="email" id="email" name="email"
                                value="{{ $reservation->email ?? '' }}"
                                class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                        </div>
                        @error('email')
                            <div class="text-sm text-red-400">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="sm:col-span-5">
                        <label for="phone" class="block text-sm font-medium text-gray-700"> Phone
                            number
                        </label>
                        <div class="mt-1">
                            <input type="number" id="phone" name="phone"
                                value="{{ $reservation->phone ?? '' }}"
                                class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                        </div>
                        @error('phone')
                            <div class="text-sm text-red-400">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="sm:col-span-5">
                        <label for="res_date" class="block text-sm font-medium text-gray-700"> 
                            Booking Date
                        </label>
                        <div class="mt-1">
                            <input type="datetime-local" id="res_date" name="res_date"
                                min="{{ $min_date->format('Y-m-d\TH:i:s') }}"
                                max="{{ $max_date->format('Y-m-d\TH:i:s') }}"
                                value="{{ $reservation ? $reservation->res_date->format('Y-m-d\TH:i:s') : '' }}"
                                class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                        </div>
                        <span class="text-xs italic">Please choose the time between 8:00AM - 5:00PM.</span>
                       
                    </div>
                    <div class="sm:col-span-5">
                        <label for="message" class="block text-sm font-medium text-gray-700"> Message
                        </label>
                        <div class="mt-1">
                            <textarea type="text" id="message" name="message" rows="3"
                              
                                class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5"> 
                                {{ $reservation->message ?? '' }}</textarea>
                        </div>
                        @error('message')
                            <div class="text-sm text-red-400">{{ $message }}</div>
                        @enderror

                    </div>
                    <div class="mt-2 p-4 flex justify-center">
                        <button type="submit"
                        class="px-8 py-2 bg-pink-600 hover:bg-pink-400 rounded-lg text-white">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    

    <footer class="bg-gray-800 border-t border-gray-200 py-12">
        <div class="w-full my-8 text-center">
        <p class="text-xl tracking-widest text-white mb-2">VISIT US ON SOCIAL NETWORKS</p>
        <h2 class="text-4xl font-extrabold tracking-widest text-white mb-20">
          FOLLOW US</h2> 
         </div>          
          <div class="flex justify-center mt-4 lg:mt-0 space-x-5">
            <a href="https://www.facebook.com/manilyn.nicolas.1" target="_blank">
              <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                class="animate-bounce w-14 h-14 text-white bg-pink-600 rounded-full hover:bg-blue-700 hover:text-white" viewBox="0 0 24 24">
                <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
              </svg>
            </a>
            <a class="ml-3">
              <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                class="animate-bounce w-14 h-14 text-white bg-pink-600 rounded-full hover:bg-blue-400 hover:text-white items-center" viewBox="0 0 26 30">
                <path
                  d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z">
                </path>
              </svg>
            </a>
            <a class="ml-3">
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                class="animate-bounce w-14 h-14 text-white bg-pink-600 rounded-full hover:bg-gradient-to-r from-violet-700 via-red-400 to-red-500 hover:text-white" viewBox="0 0 24 26">
                <rect width="18" height="16" x="3" y="5" rx="3" ry="5"></rect>
                <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
              </svg>
            </a>
            
          </div>
          <div class="flex flex-wrap items-center justify-center px-4 py-8 mx-auto mt-8">
            <p class="select-none text-white text-center text-lg tracking-wide">
              Copyright &copy; 2022 <a class="text-lg text-pink-400 tracking-wide"> Chanelle Beauty Parlor </a>
            </p>
        </div>
      </div>
      <div class="flex flex-wrap items-center justify-center">
        <p class="select-none text-white text-center text-lg tracking-wide">
          Developed by Jackelyn Corpuz 
        </p>
    </div>
      </footer>
</body>
</html>
