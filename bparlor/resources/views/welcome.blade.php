<x-guest-layout>

    <div
      class="container max-w-lg px-4 py-32 mx-auto text-left bg-center bg-no-repeat bg-cover md:max-w-none md:text-center"
      style="background-image: url('https://zuryebeauty.com/wp-content/uploads/2021/02/6ed6f98d129906dcf03e7ec031fd2963-1536x576.jpg')">  
      <br>
      <h1
        class="font-serif text-5xl font-extrabold tracking-wide text-transparent sm:text-5xl md:text-5xl lg:text-5xl xl:text-6xl bg-clip-text bg-gradient-to-r from-pink-600 to-gray-800 md:text-center sm:leading-none">
        <span class="inline md:block ">WE'LL CREATE YOUR <br> PERFECT STYLE</span>
      </h1>
      <div class="mx-auto mt-3 tracking-wide text-gray-800 md:text-center font-medium lg:text-lg drop-shadow-2xl no-underline">
        We want to settle on a style that suits the image of <br>yourself you want to have.
      </div>
      <div class="flex flex-col items-center mt-12 text-center">
        <span class="relative inline-flex w-full md:w-auto">
          <a href="{{ route('reservations.step.one') }}" type="button"
            class="inline-flex items-center trecking-wide justify-center px-6 py-2 text-base font-medium leading-6 text-white bg-pink-600 shadow-lg shadow-pink-900/50 rounded-full lg:w-full md:w-auto hover:bg-gray-800 focus:outline-none">
            MAKE AN APPOINTMENT
          </a>
      </div>
      <br>
    </div>
  
    <!-- End Main Hero Content -->
    <section id="about" class="bg-fixed bg-cover py-20" 
    style="background-image: url('https://img.freepik.com/free-vector/pink-watercolor-leaves-background_23-2148907681.jpg?w=2000')">
      <br>
      <div class="container w-full px-5 py-6 mx-auto">
        <div class=" grid lg:grid-cols-4 gap-y-6">
          <div class=" max-w-xs mx-4 mb-2 mt-6 rounded-lg">
          
            <div class="px-6 py-4">
              <div class="mb-2 ">
                <p class="select-none items-right text-30 font-bold text-gray-800">
              BEAUTY/HAIR CARE/STYLE</p>
              </div>
              <h3 class="text-3xl tracking-wider font-extrabold text-pink-600 mb-3">WHAT DO YOU NEED?</h3>
              <div class="select-none flex items-center justify-between p-2 mb-4">
                <p class="leading-normal font-medium text-gray-800 mb-4">
                  We pride ourselves on our high quality work and attention to detail. Here we offer best treatment that you might have never experienced before.</p>
      
              </div>
              <div class="relative flex">
                <a href="{{ route('services.index') }}"
                  class="flex items-center tracking-wide justify-center px-4 py-2 text-base font-semibold leading-6 text-white bg-pink-600 shadow-lg shadow-pink-900/50 rounded-full lg:w-full md:w-auto hover:bg-pink-400 focus:outline-none">
                 READ MORE
                  <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 ml-1" viewBox="0 0 24 24" fill="none"
                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                    <line x1="5" y1="12" x2="19" y2="12"></line>
                    <polyline points="12 5 19 12 12 19"></polyline>
                  </svg>
                </a>
              </div>  
            </div>
           
          </div>
          <div class="bg-white max-w-xs mx-4 mb-2 rounded-t-full rounded-r-full shadow-lg">
            <img class="w-full h-50 rounded-t-full" 
            src="https://w0.peakpx.com/wallpaper/155/978/HD-wallpaper-beauty-sensual-pretty-women-sweet-nice-she-hand-flowers-face-lovely-models-closeup-blonde-sexy-lips-hands-makeup-sex-eyes-white-red-dress-blond-beautiful-woman-elegant-hair.jpg"
              alt="Image" />
            <div class="px-6 py-4">
              <div class="flex mb-2">
                <span class="tracking-wide px-2 py-2 text-xl font-extrabold rounded-full text-gray-800 text-wide drop-shadow-2xl">
                  BEAUTY</span>
              </div>
              <div class="flex items-center justify-between p-2">
                <p class="leading-normal text-black-700">
                  Beauty and skin treatments are often deeply intimate and delicate experiences. That Is why we tend to your skin rejuvenation needs in private cubicles with floor to ceiling walls to ensure discretion at all costs. 
                    
                </p>
              </div>
          </div>
          </div>

          <div class="bg-white max-w-xs mx-4 mb-2 rounded-t-full rounded-r-full shadow-lg">
            <img class="w-full h-50 rounded-t-full " 
            src="https://media.istockphoto.com/photos/beautiful-girl-with-long-and-curly-hairs-picture-id1187559648?b=1&k=20&m=1187559648&s=170667a&w=0&h=gzavlSeobCluvCJMcOAfcCLHKgnUVda3rOAdJ9J2NFA="
              alt="Image" />
              <div class="px-6 py-4">
                <div class="flex mb-2">
                  <span class="tracking-wide px-2 py-2 text-xl font-extrabold rounded-full text-gray-800 text-wide drop-shadow-2xl">
                  HAIR STYLE</span>
              </div>
              <div class="flex items-center justify-between p-2">
                <p class="leading-normal text-black-700">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
                
              </div>
              
            </div>
          </div>

          <div class="bg-white max-w-xs mx-4 mb-2 rounded-t-full rounded-r-full shadow-lg">
            <img class="w-full h-55 rounded-t-full " 
            src="https://us.123rf.com/450wm/victorias/victorias1512/victorias151200038/50341325-hair-beautiful-brunette-girl-makeup-healthy-long-hair-beauty-model-woman-with-white-flowers-straight.jpg?ver=6"
              alt="Image" />
              <div class="px-6 py-4">
                <div class="flex mb-2">
                  <span class="select-none tracking-wide px-2 py-2 text-xl font-extrabold rounded-full text-gray-800 text-wide drop-shadow-2xl">
                  HAIR CARE</span>
              </div>
              <div class="flex items-center justify-between p-2">
                <p class="leading-normal text-black-700">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
              </div>
            </div>
          </div> 
        </div>
      </div>
    </section>

    <section class="pt-8 pb-12 bg-fixed bg-cover py-20"
    style="background-image: url('https://img.freepik.com/free-photo/chamomile-flowers-pink-background-daisies-background-with-copy-space-summer-background-chamomile-field-top-view_115594-2802.jpg?w=2000')">
      <div class="my-16 text-center">
        <h2 class="font-serif text-9xl font-extrabold tracking-wider text-gray-800 sm:text-6xl md:text-5xl lg:text-5xl xl:text-6xl bg-clip-text bg-gradient-to-r from-pink-400 to-gray-800 md:text-center sm:leading-none">
          35<a class="text-pink-700">%</a>OFF </h2>
        <p class="text-2xl font-medium tracking-widest text-gray-800 mt-14">AWESOME TOTAL SALE -35% OFF FOR ALL <br>
          <a class="font-extrabold underline decoration-pink-600">HAIR PROCEDURES</a> DURING THIS SEASON!</p>
      </div>
      <!--<div class="items-center mt-16 text-center">
        <a href="#_"
          class="text-xl tracking-wide items-center justify-center px-3 py-3 text-base font-medium leading-6 text-white bg-pink-600 shadow-lg shadow-pink-900/50 rounded-full lg:w-full md:w-auto hover:bg-gray-800 focus:outline-none">
          PURCHASE NOW!
        </a>
      </div>  -->
      
    </section>

    <section class="bg-fixed bg-cover py-20" 
      style="background-image: url('https://img.freepik.com/free-vector/pink-watercolor-leaves-background_23-2148907681.jpg?w=2000')">
      <div class="container items-center max-w-6xl px-4 px-10 mx-auto sm:px-20 md:px-32 lg:px-16">      
        <div class="flex flex-wrap items-center -mx-3">
          <div class="order-1 w-full px-3 lg:w-1/2 lg:order-0">
            <div class="w-full lg:max-w-md">
              <h2
                class="mb-4 text-4xl tracking-wider font-extrabold bg-clip-text text-pink-600">
                WHY CHOOSE US?</h2>
              
              <p class="leading-normal mb-4 font-medium tracking-normal text-gray-800 xl:mb-6">
                You are our first priority and we are dedicated to providing you with exceptional service and effective treatments. 
                It does not matter if you are our first client of the day or the last - your needs are paramount. There is no rush and absolutely no compromising.</p>
              <ul>
                <li class="flex items-center py-2 space-x-4 xl:py-3 bg-pink-100 rounded-r-full shadow-lg">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="w-20 h-20 text-pink-600" fill="#DB7093">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M182.6 246.6C170.1 259.1 149.9 259.1 137.4 246.6L57.37 166.6C44.88 154.1 44.88 133.9 57.37 121.4C69.87 108.9 90.13 108.9 102.6 121.4L159.1 178.7L297.4 41.37C309.9 28.88 330.1 28.88 342.6 41.37C355.1 53.87 355.1 74.13 342.6 86.63L182.6 246.6zM182.6 470.6C170.1 483.1 149.9 483.1 137.4 470.6L9.372 342.6C-3.124 330.1-3.124 309.9 9.372 297.4C21.87 284.9 42.13 284.9 54.63 297.4L159.1 402.7L393.4 169.4C405.9 156.9 426.1 156.9 438.6 169.4C451.1 181.9 451.1 202.1 438.6 214.6L182.6 470.6z"/>
                  </svg>
                  <span class="text-2xl font-extrabold tracking-wider text-gray-800">100% QUALITY 
                    <h1 class="text-2xl font-normal text-gray-800">PRODUCTS & SERVICE</h1>
                  </span>
                </li>

               <li class="flex items-center py-2 space-x-4 xl:py-3 mt-2 bg-pink-50 rounded-r-full shadow-lg">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="w-20 h-20 text-pink-600" fill="#DB7093">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" 
                    d="M572.1 82.38C569.5 71.59 559.8 64 548.7 64h-100.8c.2422-12.45 .1078-23.7-.1559-33.02C447.3 13.63 433.2 0 415.8 0H160.2C142.8 0 128.7 13.63 128.2 30.98C127.1 40.3 127.8 51.55 128.1 64H27.26C16.16 64 6.537 71.59 3.912 82.38C3.1 85.78-15.71 167.2 37.07 245.9c37.44 55.82 100.6 95.03 187.5 117.4c18.7 4.805 31.41 22.06 31.41 41.37C256 428.5 236.5 448 212.6 448H208c-26.51 0-47.99 21.49-47.99 48c0 8.836 7.163 16 15.1 16h223.1c8.836 0 15.1-7.164 15.1-16c0-26.51-21.48-48-47.99-48h-4.644c-23.86 0-43.36-19.5-43.36-43.35c0-19.31 12.71-36.57 31.41-41.37c86.96-22.34 150.1-61.55 187.5-117.4C591.7 167.2 572.9 85.78 572.1 82.38zM77.41 219.8C49.47 178.6 47.01 135.7 48.38 112h80.39c5.359 59.62 20.35 131.1 57.67 189.1C137.4 281.6 100.9 254.4 77.41 219.8zM498.6 219.8c-23.44 34.6-59.94 61.75-109 81.22C426.9 243.1 441.9 171.6 447.2 112h80.39C528.1 135.7 526.5 178.7 498.6 219.8z"/>
                  </svg>
                  <span class="text-2xl font-extrabold tracking-wider text-gray-800">PROFESSIONAL 
                    <h1 class="text-2xl font-normal text-gray-800">BEAUTICIAN</h1>
                  </span>
                </li>

                <li class="flex items-center py-2 space-x-4 xl:py-3 mt-2 bg-pink-100 rounded-r-full shadow-lg">
                  <svg xmlns="http://www.w3.org/2000/svg" class="w-20 h-20 text-pink-600" viewBox="0 0 512 512" fill="#DB7093">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M374.6 73.39c-12.5-12.5-32.75-12.5-45.25 0l-320 320c-12.5 12.5-12.5 32.75 0 45.25C15.63 444.9 23.81 448 32 448s16.38-3.125 22.62-9.375l320-320C387.1 106.1 387.1 85.89 374.6 73.39zM64 192c35.3 0 64-28.72 64-64S99.3 64.01 64 64.01S0 92.73 0 128S28.7 192 64 192zM320 320c-35.3 0-64 28.72-64 64s28.7 64 64 64s64-28.72 64-64S355.3 320 320 320z"/>
                  </svg>
                  <span class="text-2xl font-extrabold tracking-wider text-gray-800">SPECIAL OFFERS 
                    <h1 class="text-2xl font-normal text-gray-800">FOR YOU</h1>
                  </span>
                </li>
              </ul>
            </div>
          </div>
          <div class="w-full px-1 mb-8 lg:w-1/2 order-0 lg:order-1 lg:mb-0 py-20">
            <img class="mx-auto sm:max-w-sm lg:max-w-full "
              src="https://www.pngplay.com/wp-content/uploads/8/Women-Hair-Transparent-Image.png" alt="feature image">
            </div>
        </div>
      </div>
    </section>
   

    <!--<section id="services" class="bg-fixed bg-cover py-20" 
    style="background-image: url('https://img.freepik.com/free-vector/pink-watercolor-leaves-background_23-2148907681.jpg?w=2000')">
      <div class="tracking-wide text-center"><br>
      <h3 class="text-xl font-normal italic">Our Special</h3>
        <h1 class="text-5xl font-bold text-gray-800 bg-clip-text bg-gradient-to-r from-green-400 to-blue-500">
           S E R V I C E S</h1>
      </div>
      <div class="container w-full px-5 py-6 mx-auto">
        <div class=" grid lg:grid-cols-4 gap-y-6">
          <div class="bg-white max-w-xs mx-4 mb-2 rounded-lg shadow-lg">
            <img class="w-full h-60" src="https://lh3.googleusercontent.com/p/AF1QipNNRTwQFQSHjxT0Kty5FbBPtIeZmfhYQhTXfzsC=w600-h0"
              alt="Image" />
            <div class="px-6 py-4">
              <div class="flex mb-2">
                <span class="select-none px-4 py-2 text-20 font-bold bg-gradient-to-r from-pink-400 to-pink-700 text-white rounded-t-full rounded-r-full shadow-lg">
                  Hair Straightening</span> 
              </div>
              <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Hair Relax</h4>
                <span class="text-lg text-pink-600">P900</span>
              </div>
              <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Hair Rebond</h4>
                <span class="text-lg text-pink-600">P1,200</span>
              </div>
               <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Butox Rebond</h4>
                <span class="text-lg text-pink-600">P2,500</span>
              </div>
               <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Brazillian Rebond</h4>
                <span class="text-lg text-pink-600">P2,000</span>
              </div>
              <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Keratin Hotoil</h4>
                <span class="text-lg text-pink-600">P1,500</span>
              </div>
              
            </div>
           
          </div>
          <div class="bg-white max-w-xs mx-4 mb-2 rounded-lg shadow-lg">
            <img class="w-full h-60" src="https://blog.wella.com/sites/default/files/small-image/wellaprofessionalspastelpinkhaircolor.jpg"
              alt="Image" />
            <div class="px-6 py-4">
              <div class="flex mb-2">
                <span class="select-none px-4 py-2 text-20 font-bold bg-gradient-to-r from-pink-400 to-pink-700 text-white rounded-t-full rounded-r-full shadow-lg">
                  Hair Color</span>
              </div>
              <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Partial Highlight</h4>
                <span class="text-lg text-pink-600">P500</span>
              </div>
              <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Partial with Color</h4>
                <span class="text-lg text-pink-600">P750</span>
              </div>
              <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Full Highlight</h4>
                <span class="text-lg text-pink-600">P1,200</span>
              </div>
              <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Full with Color</h4>
                <span class="text-lg text-pink-600">P1,500</span>
              </div>
              <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Root Touch</h4>
                <span class="text-lg text-pink-600">P1,000</span>
              </div>
          </div>
          </div>

          <div class="bg-white max-w-xs mx-4 mb-2 rounded-lg shadow-lg">
            <img class="w-full h-60" src="https://rafaelsbarbershop.com/storage/photos/1/posts/mens-haircuts/mens-haircuts-1.jpg"
              alt="Image" />
            <div class="px-6 py-4">
              <div class="flex mb-2">
                <span class="select-none px-4 py-2 text-20 font-bold bg-gradient-to-r from-pink-400 to-pink-700 text-white rounded-t-full rounded-r-full shadow-lg">
                  Haircut</span>
              </div>
             <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Women's Haircut</h4>
                <span class="text-lg text-pink-600">P100</span>
              </div>
              <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Men's Haircut</h4>
                <span class="text-lg text-pink-600">P75</span>
              </div>
              <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Child's Haircut</h4>
                <span class="text-lg text-pink-600">P50</span>
              </div>
              <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Wash and Style</h4>
                <span class="text-lg text-pink-600">P300</span>
              </div>
              <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Iron/Curling</h4>
                <span class="text-lg text-pink-600">P400</span>
              </div>
            </div>
          </div>

          <div class="bg-white max-w-xs mx-4 mb-2 rounded-lg shadow-lg">
            <img class="w-full h-60" src="https://dynl.mktgcdn.com/p/LqisxTNCyxPulXpDcBAbkUSdSXd-rzBgv2od3up2nlo/619x619.jpg"
              alt="Image" />
            <div class="px-6 py-4">
              <div class="flex mb-2">
                <span class="select-none px-4 py-2 text-20 font-bold bg-gradient-to-r from-pink-400 to-pink-700 text-white rounded-t-full rounded-r-full shadow-lg">
                  Make Up Service</span>
              </div>
              <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Eye Makeup</h4>
                <span class="text-lg text-pink-600">P150</span>
              </div>
              <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Full Makeup</h4>
                <span class="text-lg text-pink-600">P300</span>
              </div>
              <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Hair & Makeup</h4>
                <span class="text-lg text-pink-600">P500</span>
              </div>
              <div class="select-none hover:underline transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:font-bold duration-150 flex items-center justify-between p-2">
                <h4 class="leading-normal text-black-700">Airbrush Makeup</h4>
                <span class="text-lg text-pink-600">P400</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> -->

    <!--<section id="gallery" class="pt-4 pb-12 bg-white">
      <div class="my-8 text-center">
        <h2 class="text-5xl font-bold text-gray-800 bg-clip-text bg-gradient-to-r from-green-400 to-blue-500">
          G A L L E R Y</h2>
        <p class="text-xl italic">Beauty in real life.</p>
      </div>
      <div class="container grid gap-0 mx-auto lg:grid-cols-4">
        <div class="w-full rounded transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-150">
          <img src="https://media.istockphoto.com/photos/makeup-artist-paints-model-in-beauty-studio-picture-id681346362?k=20&m=681346362&s=612x612&w=0&h=8AxjuymeGLHvqLNqCK2eJ0AEiKB0CvygjS0r32ECRmk=" alt="image">
        </div>
        <div class="w-full rounded transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-150">
          <img src="https://media.istockphoto.com/photos/top-view-of-client-sitting-close-to-sink-picture-id1339268647?b=1&k=20&m=1339268647&s=170667a&w=0&h=fz2acI25iew9UDI_9evWsYXgNPK0-pHSJqaYXwntqnw=" alt="image">
        </div>
        <div class="w-full rounded transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-150">
          <img src="https://cdn.create.vista.com/api/media/medium/231316190/stock-photo-young-female-hairdresser-combing-hair?token=" alt="image">
        </div>
        <div class="w-full rounded transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-150">
          <img src="https://media.istockphoto.com/photos/the-hairdresser-is-using-comb-for-female-pink-hair-at-beauty-salon-picture-id1182744304?k=20&m=1182744304&s=612x612&w=0&h=5HQPcBRtj61niMRoi8_WblsRSC7IRRd5IumlnUG6ue4=" alt="image">
        </div>
        <div class="w-full rounded transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-150">
          <img src="https://st2.depositphotos.com/1046535/8168/i/450/depositphotos_81686546-stock-photo-woman-receiving-haircut-from-hair.jpg" alt="image">
        </div>
        <div class="w-full rounded transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-150">
          <img src="https://media.istockphoto.com/photos/stylist-drying-girls-hair-with-hair-dryer-in-beauty-salon-picture-id1165466432?k=20&m=1165466432&s=612x612&w=0&h=fq2zkWqA60PBz9TGBnbRo_9ypQuMmrj-u7dsxfn2gw8=" alt="image">
        </div>
        <div class="w-full rounded transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-150">
          <img src="https://media.istockphoto.com/photos/young-brown-haired-beautiful-model-with-long-straight-well-groomed-picture-id896422106?k=20&m=896422106&s=612x612&w=0&h=4drfxZEWEGqFmfYxDAcU16Tkiudbar9W-qFtNzQw0V8=" alt="image">
        </div>
        <div class="w-full rounded transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-150">
          <img src="https://d3hgq0oz43fepq.cloudfront.net/assets/files/1781/professional-hair-coloring-k-bella-brighton.770x0.jpg" alt="image">
        </div><br>
      </div>
  
    </section>-->

     <!-- <section class="pt-4 pb-12 bg-gray-800">
      <div class="my-16 text-center">
        <h2 class="text-5xl font-bold tracking-widest text-white bg-clip-text bg-gradient-to-r from-green-400 to-blue-500">
          CLIENTS TESTIMONIALS </h2>
        <p class="text-2xl text-white italic">Happy clients about us.</p>
      </div>
      <br>
      <div class="grid gap-2 lg:grid-cols-3">
        <div class="max-w-md p-4 bg-white rounded-lg shadow-lg">
          <div class="flex justify-center -mt-16 md:justify-end">
            <img class="object-cover animate-bounce w-20 h-20 border-2 border-pink-500 rounded-full"
              src="https://images.unsplash.com/photo-1499714608240-22fc6ad53fb2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80">
          </div>
          <div>
            <p class="mt-2 text-gray-600">"Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae dolores deserunt
              ea doloremque natus error, rerum quas odio quaerat nam ex commodi hic, suscipit in a veritatis pariatur
              minus consequuntur!"</p>
          </div>
          <div class="flex justify-end mt-4">
            <p href="#" class="text-xl font-medium text-pink-600 italic">- John Domak</p>
          </div>
        </div>
        <div class="max-w-md p-4 bg-white rounded-lg shadow-lg">
          <div class="flex justify-center -mt-16 md:justify-end">
            <img class="object-cover animate-bounce w-20 h-20 border-2 border-pink-500 rounded-full"
              src="https://cdn.pixabay.com/photo/2018/01/04/21/15/young-3061652__340.jpg">
          </div>
          <div>
            <p class="mt-2 text-gray-600">"Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae dolores deserunt
              ea doloremque natus error, rerum quas odio quaerat nam ex commodi hic, suscipit in a veritatis pariatur
              minus consequuntur!"</p>
          </div>
          <div class="flex justify-end mt-4">
            <a href="#" class="text-xl font-medium text-pink-600 italic">- Maria del Barrio</a>
          </div>
        </div>
        <div class="max-w-md p-4 bg-white rounded-lg shadow-lg">
          <div class="flex justify-center -mt-16 md:justify-end">
            <img class="object-cover animate-bounce w-20 h-20 border-2 border-pink-500 rounded-full"
              src="https://cdn.pixabay.com/photo/2018/01/18/17/48/purchase-3090818__340.jpg">
          </div>
          <div>
            <p class="mt-2 text-gray-600">"Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae dolores deserunt
              ea doloremque natus error, rerum quas odio quaerat nam ex commodi hic, suscipit in a veritatis pariatur
              minus consequuntur!"</p>
          </div>
          <div class="flex justify-end mt-4">
            <a href="#" class="text-xl font-medium text-pink-600 italic">- Bella Corpuz</a>
          </div>
          <br>
        </div>
      </div>
    </section> -->

   <!-- <section id="contact" class="bg-white py-20">
    <div class="container items-center mx-auto">      
      <div class="flex flex-wrap items-center -mx-3">
        <div class="gap-2 columns-1 order-1 w-full px-3 lg:w-1/2 lg:order-0">
          <div class="w-full lg:max-w-md ml-14 my-12 bg-pink-100 max-w-xs mx-4 mb-2 mt-4 rounded-r-full shadow-lg">
            <h2
              class="text-4xl tracking-widest font-extrabold bg-clip-text text-pink-600 mb-6">
              CONTACT US</h2>
            
              <p class="text-xl tracking-widest text-gray-800 mb-10">HAVE A QUESTION?</p>
          
            <ul>
              <li class="flex items-center py-2 space-x-4 xl:py-3">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="w-8 h-8 text-white" fill="#DB7093">
                  <path d="M408 120C408 174.6 334.9 271.9 302.8 311.1C295.1 321.6 280.9 321.6 273.2 311.1C241.1 271.9 168 174.6 168 120C168 53.73 221.7 0 288 0C354.3 0 408 53.73 408 120zM288 152C310.1 152 328 134.1 328 112C328 89.91 310.1 72 288 72C265.9 72 248 89.91 248 112C248 134.1 265.9 152 288 152zM425.6 179.8C426.1 178.6 426.6 177.4 427.1 176.1L543.1 129.7C558.9 123.4 576 135 576 152V422.8C576 432.6 570 441.4 560.9 445.1L416 503V200.4C419.5 193.5 422.7 186.7 425.6 179.8zM150.4 179.8C153.3 186.7 156.5 193.5 160 200.4V451.8L32.91 502.7C17.15 508.1 0 497.4 0 480.4V209.6C0 199.8 5.975 190.1 15.09 187.3L137.6 138.3C140 152.5 144.9 166.6 150.4 179.8H150.4zM327.8 331.1C341.7 314.6 363.5 286.3 384 255V504.3L192 449.4V255C212.5 286.3 234.3 314.6 248.2 331.1C268.7 357.6 307.3 357.6 327.8 331.1L327.8 331.1z"/>
                </svg>
                <p class="text-base font-medium tracking-wide text-gray-800">
                  Location: Asingan, 2438 Pangasinan, Philippines
                </p>
              </li>    

              <li class="flex items-center py-2 space-x-4 xl:py-3">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="w-8 h-8 text-white" fill="#DB7093">
                  <path d="M511.2 387l-23.25 100.8c-3.266 14.25-15.79 24.22-30.46 24.22C205.2 512 0 306.8 0 54.5c0-14.66 9.969-27.2 24.22-30.45l100.8-23.25C139.7-2.602 154.7 5.018 160.8 18.92l46.52 108.5c5.438 12.78 1.77 27.67-8.98 36.45L144.5 207.1c33.98 69.22 90.26 125.5 159.5 159.5l44.08-53.8c8.688-10.78 23.69-14.51 36.47-8.975l108.5 46.51C506.1 357.2 514.6 372.4 511.2 387z"/>
                </svg>
                <p class="text-base font-medium tracking-wide text-gray-800">
                  Phone: +639 985 1234 789
                </p>
              </li>  
              
              <li class="flex items-center py-2 space-x-4 xl:py-3">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="w-8 h-8 text-white" fill="#DB7093">
                  <path d="M256 417.1c-16.38 0-32.88-4.1-46.88-15.12L0 250.9v213.1C0 490.5 21.5 512 48 512h416c26.5 0 48-21.5 48-47.1V250.9l-209.1 151.1C288.9 412 272.4 417.1 256 417.1zM493.6 163C484.8 156 476.4 149.5 464 140.1v-44.12c0-26.5-21.5-48-48-48l-77.5 .0016c-3.125-2.25-5.875-4.25-9.125-6.5C312.6 29.13 279.3-.3732 256 .0018C232.8-.3732 199.4 29.13 182.6 41.5c-3.25 2.25-6 4.25-9.125 6.5L96 48c-26.5 0-48 21.5-48 48v44.12C35.63 149.5 27.25 156 18.38 163C6.75 172 0 186 0 200.8v10.62l96 69.37V96h320v184.7l96-69.37V200.8C512 186 505.3 172 493.6 163zM176 255.1h160c8.836 0 16-7.164 16-15.1c0-8.838-7.164-16-16-16h-160c-8.836 0-16 7.162-16 16C160 248.8 167.2 255.1 176 255.1zM176 191.1h160c8.836 0 16-7.164 16-16c0-8.838-7.164-15.1-16-15.1h-160c-8.836 0-16 7.162-16 15.1C160 184.8 167.2 191.1 176 191.1z"/>
                </svg>
                <p class="select-none text-base font-medium tracking-wide text-gray-800">
                  Email: <a class="text-pink-700 underline decoration-pink-700 hover:text-gray-700">chanellebeauty@gmail.com</a>
                </p>
              </li>   

              <li class="flex items-center py-2 space-x-4 xl:py-3">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="w-8 h-8 text-white" fill="#DB7093">
                  <path d="M352 256C352 278.2 350.8 299.6 348.7 320H163.3C161.2 299.6 159.1 278.2 159.1 256C159.1 233.8 161.2 212.4 163.3 192H348.7C350.8 212.4 352 233.8 352 256zM503.9 192C509.2 212.5 512 233.9 512 256C512 278.1 509.2 299.5 503.9 320H380.8C382.9 299.4 384 277.1 384 256C384 234 382.9 212.6 380.8 192H503.9zM493.4 160H376.7C366.7 96.14 346.9 42.62 321.4 8.442C399.8 29.09 463.4 85.94 493.4 160zM344.3 160H167.7C173.8 123.6 183.2 91.38 194.7 65.35C205.2 41.74 216.9 24.61 228.2 13.81C239.4 3.178 248.7 0 256 0C263.3 0 272.6 3.178 283.8 13.81C295.1 24.61 306.8 41.74 317.3 65.35C328.8 91.38 338.2 123.6 344.3 160H344.3zM18.61 160C48.59 85.94 112.2 29.09 190.6 8.442C165.1 42.62 145.3 96.14 135.3 160H18.61zM131.2 192C129.1 212.6 127.1 234 127.1 256C127.1 277.1 129.1 299.4 131.2 320H8.065C2.8 299.5 0 278.1 0 256C0 233.9 2.8 212.5 8.065 192H131.2zM194.7 446.6C183.2 420.6 173.8 388.4 167.7 352H344.3C338.2 388.4 328.8 420.6 317.3 446.6C306.8 470.3 295.1 487.4 283.8 498.2C272.6 508.8 263.3 512 255.1 512C248.7 512 239.4 508.8 228.2 498.2C216.9 487.4 205.2 470.3 194.7 446.6H194.7zM190.6 503.6C112.2 482.9 48.59 426.1 18.61 352H135.3C145.3 415.9 165.1 469.4 190.6 503.6V503.6zM321.4 503.6C346.9 469.4 366.7 415.9 376.7 352H493.4C463.4 426.1 399.8 482.9 321.4 503.6V503.6z"/>
                </svg>
                <p class="select-none text-base font-medium tracking-wide">
                  Website: <a class="text-pink-700 underline decoration-pink-700 hover:text-gray-700">www.chanellebeauty.com</a>
                </p> <br>
              </li>   
              
            </ul>            
          </div>         
        </div>
       <div class="w-full mx-auto px-2 lg:w-1/2 order-0 lg:order-1 lg:mb-0 py-20 bg-cover "
       style="background-image: url('https://map.viamichelin.com/map/carte?map=viamichelin&z=10&lat=16.0035&lon=120.66861&width=550&height=382&format=png&version=latest&layer=background&debug_pattern=.*')">     
       <div class="my-8 text-center">
            <p class="text-xl text-transparent">VISIT US ON SOCIAL NETWORKS</p>
            <h2 class="text-5xl font-bold text-transparent">
              .</h2> 
          </div>
          <br><br><br><br>
        </div>
      </div>
    </div>
    </section>-->

   

</x-guest-layout>