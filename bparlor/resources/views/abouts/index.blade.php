<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'ChanelleBeauty') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body>
        <div class="sticky top-0 bg-white shadow-md" x-data="{ isOpen: false }">
            <nav class="container px-6 py-3 mx:auto md:flex md:justify-between md:items-center">
              <div class="flex items-center justify-between">
                <a class="text-xl font-bold text-transparent bg-clip-text bg-gradient-to-r from-pink-600 to-gray-700 md:text-2xl hover:text-pink-400 border-solid border-2 border-b-pink-600 border-r-pink-600 border-l-gray-600 border-t-gray-600"
                  href="#">
                  ChanelleBeauty.     
                </a> 
                <!-- Mobile menu button -->
                <div @click ="isOpen = !isOpen" class="flex md:hidden">
                  <button type="button" class="text-gray-800 hover:text-gray-400 focus:outline-none focus:text-gray-400"
                    aria-label="toggle menu">
                    <svg viewBox="0 0 24 24" class="w-6 h-6 fill-current">
                      <path fill-rule="evenodd"
                        d="M4 5h16a1 1 0 0 1 0 2H4a1 1 0 1 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2z">
                      </path>
                    </svg>
                  </button>
                </div>
              </div>
      
              <!-- Mobile Menu open: "block", Menu closed: "hidden" -->
              <div :class="isOpen ? 'flex' : 'hidden'"
                class="flex-col mt-8 space-y-4 md:flex md:space-y-0 md:flex-row md:items-center md:space-x-10 md:mt-0">
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="/">Home</a>
                <a class="text-gray-800 font-semibold bg-clip-text text-pink-600 underline"
                  href="{{ route('abouts.index') }}">About</a>
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="{{ route('services.index') }}">Services</a>
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="{{ route('galleries.index') }}">Gallery</a>
                <a class="text-gray-800 font-semibold bg-clip-text  bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="{{ route('reservations.step.one') }}">Appointment</a>
                <a class="text-gray-800 font-semibold bg-clip-text bg-gradient-to-r from-pink-400 to-blue-500 hover:text-pink-600 no-underline hover:underline"
                  href="{{ route('contacts.index') }}">Contact</a>
              </div>
            </nav>
          </div>


          <section id="about"  class="bg-fixed bg-cover py-20" 
          style="background-image: url('https://img.freepik.com/free-vector/pink-watercolor-leaves-background_23-2148907681.jpg?w=2000')">
         
          <div class="container items-center max-w-6xl px-4 px-10 mx-auto sm:px-20 md:px-32 lg:px-16">      
            <div class="flex flex-wrap items-center -mx-3">
              <div class="order-1 w-full px-3 lg:w-1/2 lg:order-0">
                <div class="w-full lg:max-w-md">
                  <h2
                  class="mb-4 text-4xl font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-pink-600 to-gray-700">
                  About Us</h2>
                  @foreach ($abouts as $about)
                  <p class="leading-normal text-20 mb-8 font-medium tracking-normal text-gray-800 xl:mb-6">
                    {{ $about->about_us }}
                  </p>
                  @endforeach
                </div>
              </div>
              <div class="w-full px-1 mb-8 lg:w-1/2 order-0 lg:order-1 lg:mb-0 py-20">
                <img class="mx-auto sm:max-w-sm lg:max-w-full rounded-t-full rounded-r-full shadow-lg"
                  src="{{ Storage::url($about->image) }}" alt="image" />
                </div>
                
            </div>
          </div>
        </section>

      

        <footer class="bg-gray-800 border-t border-gray-200 py-12">
        <div class="w-full my-8 text-center">
        <p class="text-xl tracking-widest text-white mb-2">VISIT US ON SOCIAL NETWORKS</p>
        <h2 class="text-4xl font-extrabold tracking-widest text-white mb-20">
          FOLLOW US</h2> 
         </div>          
          <div class="flex justify-center mt-4 lg:mt-0 space-x-5">
            <a href="https://www.facebook.com/manilyn.nicolas.1" target="_blank">
              <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                class="animate-bounce w-14 h-14 text-white bg-pink-600 rounded-full hover:bg-blue-700 hover:text-white" viewBox="0 0 24 24">
                <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
              </svg>
            </a>
            <a class="ml-3">
              <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                class="animate-bounce w-14 h-14 text-white bg-pink-600 rounded-full hover:bg-blue-400 hover:text-white items-center" viewBox="0 0 26 30">
                <path
                  d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z">
                </path>
              </svg>
            </a>
            <a class="ml-3">
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                class="animate-bounce w-14 h-14 text-white bg-pink-600 rounded-full hover:bg-gradient-to-r from-violet-700 via-red-400 to-red-500 hover:text-white" viewBox="0 0 24 26">
                <rect width="18" height="16" x="3" y="5" rx="3" ry="5"></rect>
                <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
              </svg>
            </a>
            <a class="ml-3">
              <svg fill="currentColor" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="0" 
                class="animate-bounce w-14 h-14 text-base text-white bg-pink-600 rounded-full hover:bg-blue-500 hover:text-white" viewBox="0 0 24 30">
                <path stroke="none"
                  d="M16 8a6 6 0 016 6v7h-4v-7a2 2 0 00-2-2 2 2 0 00-2 2v7h-4v-7a6 6 0 016-6zM2 9h4v12H2z"></path>
                <circle cx="4" cy="4" r="2" stroke="none"></circle>
              </svg>
            </a>
          </div>
          <div class="flex flex-wrap items-center justify-center px-4 py-8 mx-auto mt-8">
            <p class="select-none text-white text-center text-lg tracking-wide">
              Copyright &copy; 2022 <a class="text-lg text-pink-400 tracking-wide"> Chanelle Beauty Parlor </a>
            </p>
        </div>
        <div class="flex flex-wrap items-center justify-center">
          <p class="select-none text-white text-center text-lg tracking-wide">
            Developed by Jackelyn Corpuz 
          </p>
      </div>
      </footer>
</body>
</html>
