<?php

use App\Http\Controllers\Admin\AboutController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ContactController;
use App\Http\Controllers\Admin\CopyrightController;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\GalleryController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\PromoController;
use App\Http\Controllers\Admin\TableController;
use App\Http\Controllers\Admin\ReservationController;
use App\Http\Controllers\Admin\ServiceController;
use App\Http\Controllers\Admin\TitleController;
use App\Http\Controllers\Frontend\GalleryController as FrontendGalleryController;
use App\Http\Controllers\Frontend\ContactController as FrontendContactController;
use App\Http\Controllers\Frontend\CategoryController as FrontendCategoryController;
use App\Http\Controllers\Frontend\MenuController as FrontendMenuController;
use App\Http\Controllers\Frontend\ReservationController as FrontendReservationController;
use App\Http\Controllers\Frontend\ServiceController as FrontendServiceController;
use App\Http\Controllers\Frontend\AboutController as FrontendAboutController;
use App\Http\Controllers\Frontend\CopyrightController as FrontendCopyrightController;
use App\Http\Controllers\Frontend\PromoController as FrontendPromoController;
use App\Http\Controllers\Frontend\TitleController as FrontendTitleController;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
});


Route::get('/galleries', [FrontendGalleryController::class, 'index'])->name('galleries.index');
Route::get('/contacts', [FrontendContactController::class, 'index'])->name('contacts.index');
Route::get('/abouts', [FrontendAboutController::class, 'index'])->name('abouts.index');
Route::get('/titles', [FrontendTitleController::class, 'index'])->name('titles.index');
Route::get('/promos', [FrontendPromoController::class, 'index'])->name('promos.index');
Route::get('/copyrights', [FrontendCopyrightController::class, 'index'])->name('copyrights.index');
Route::get('/services', [FrontendServiceController::class, 'index'])->name('services.index');
Route::get('/categories', [FrontendCategoryController::class, 'index'])->name('categories.index');
Route::get('/categories{category}', [FrontendCategoryController::class, 'index'])->name('categories.show');
Route::get('/menus', [FrontendMenuController::class, 'index'])->name('menus.index');
Route::get('/reservations/step-one', [FrontendReservationController::class, 'stepOne'])->name('reservations.step.one');
Route::post('/reservations/step-one', [FrontendReservationController::class, 'storeStepOne'])->name('reservations.store.step.one');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::middleware(['auth','admin'])->name('admin.')->prefix('admin')->group(function() {
    Route::get('/', [AdminController::class, 'index'])->name('index');
    Route::resource('/categories', CategoryController::class);
    Route::resource('/menus', MenuController::class);
    Route::resource('/tables', TableController::class);
    Route::resource('/reservations', ReservationController::class);
    Route::resource('/services', ServiceController::class);
    Route::resource('/customers', CustomerController::class);
    Route::resource('/titles', TitleController::class);
    Route::resource('/abouts', AboutController::class);
    Route::resource('/promos', PromoController::class);
    Route::resource('/copyrights', CopyrightController::class);
    Route::resource('/galleries', GalleryController::class);
    Route::resource('/contacts', ContactController::class);
    Route::resource('/users', AdminController::class);
});

require __DIR__. '/auth.php';
