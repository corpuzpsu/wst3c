<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Reservation;
use App\Rules\DateBetween;
use App\Rules\TimeBetween;
use Carbon\Carbon;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
   public function stepOne(Request $request)
   {
       $reservation = $request->session()->get('reservation');
       $min_date = Carbon::today();
       $max_date = Carbon::now()->addMonth();
       return view('reservations.step-one', compact('reservation', 'min_date','max_date'));
   }

   public function storeStepOne(Request $request)
   {
       $validated = $request->validate([
        'first_name' => ['required'],
        'last_name' => ['required'],
        'email' => ['required', 'email'],
        'res_date' => ['required', 'date', new DateBetween, new TimeBetween],
        'phone' => ['required'],
        'service' => ['required'],
       ]);

       if(empty($request->session()->get('reservation'))){
           $reservation = new Reservation();
           $reservation->fill($validated);
           $request->session()->put('reservation', $reservation);
       } else {
           $reservation = $request->session()->get('reservation');
           $reservation->fill($validated);
           $request->session()->put('reservation', $reservation);
       }

       $reservation= $request->session()->get('reservation');
       //$reservation->fill($validated);
       $reservation->save();
       $request->session()->forget('reservation');

       return to_route('reservations.step.one');
    }
}
