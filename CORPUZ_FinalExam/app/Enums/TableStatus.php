<?php

namespace App\Enums;

enum TableStatus: string
{
    case Front = 'front';
    case Inside = 'inside';
    case Outside = 'outside';
}