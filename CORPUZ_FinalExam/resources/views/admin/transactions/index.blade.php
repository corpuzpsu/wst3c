<x-admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    
<div class="relative overflow-x-auto shadow-md sm:rounded-lg">
    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="px-6 py-3">
                    User ID
                </th>
                <th scope="col" class="px-6 py-3">
                    Product ID
                </th>
                <th scope="col" class="px-6 py-3">
                    Quantity
                </th>
                <th scope="col" class="px-6 py-3">
                    Date Purchased
                </th>
                <th scope="col" class="px-6 py-3">
                    Total Sales
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($transactions as $transaction)
                        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                            <td class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap dark:text-white">
                                {{ $transaction->user_id }}
                            </td>
                            <td class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap dark:text-white">
                                {{ $transaction->project_id }}
                            </td>
                            
                            <td class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap dark:text-white">
                                {{ $transaction->quantity }}
                            </td>
                            <td class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap dark:text-white">
                                {{ $transaction->created_at }}
                            </td>
                           
                        </tr>
                        @endforeach
        </tbody>
    </table>
    </div>
    
    </div>
    </div>
</x-admin-layout>
