<x-admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">   
            <div class="flex m-2 p-2">
                <a href="{{ route('admin.reservations.index') }}" 
                class="px-4 py-2 bg-slate-500 hover:bg-slate-700 rounded-lg text-white">Appointment Index</a>
            </div>
            <div class="m-2 p-2 bg-slate-100 rounded">
                <div class="space-y-8 divide-y divide-gray-200 w-1/2 mt-10">
                    <form method="POST" action="{{ route('admin.reservations.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="sm:col-span-6">
                        <label for="first_name" class="block text-sm font-medium text-gray-700"> First Name </label>
                            <div class="mt-1">
                                <input type="text" id="first_name" name="first_name" 
                                    class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                            </div>
                        
                        <label for="last_name" class="block text-sm font-medium text-gray-700"> Last Name </label>
                            <div class="mt-1">
                                <input type="text" id="last_name" name="last_name" 
                                    class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                            </div>
                        
                        <label for="email" class="block text-sm font-medium text-gray-700"> Email </label>
                        <div class="mt-1">
                            <input type="email" id="email" name="email" 
                                class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                        </div>
                        <label for="phone" class="block text-sm font-medium text-gray-700"> Phone Number </label>
                        <div class="mt-1">
                            <input type="number" id="phone" name="phone" 
                                class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                        </div>
                        <label for="res_date" class="block text-sm font-medium text-gray-700"> Date </label>
                        <div class="mt-1">
                            <input type="datetime-local" id="res_date" name="res_date" 
                                class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                        </div>
                        
                        <label for="service" class="block text-sm font-medium text-gray-700"> Service </label>
                        <div class="mt-1">
                            <input type="text" id="service" name="service" 
                                class="block w-full transition-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                        </div>
 
                        <div class="mt-5 p-2">
                            <button type="submit" 
                                class="px-4 py-2 bg-blue-600 hover:bg-blue-500 rounded-lg text-white">Add</button>
                        </div>
                    </form>
                </div>

            </div>
            
        </div>  
    </div>
</x-admin-layout>
