<?php
	session_start();
	if(!isset($_SESSION['loginuser']))
	{
		header('Location: index.php');
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Home Page</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
		<link href="css/style.css" rel="stylesheet">
		<style>
			* {
				font-family: "Century Gothic", sans-serif;
				padding: 0px;
				box-sizing: border-box;
				margin: 0px;
			}
	
			.login-form{
				width: 550px;
				margin: 10px auto;
				padding: 30px;
				border: 3px solid white;
			}
	
			body {
				background: #1e1e1f;
			}
		</style>
	</head>
	<body>
		<?php
			if(isset($_POST['logoutsr']))
			{
				session_unset();
				echo "<script type='text/javascript'>location.href='index.php';</script>";
			}
			date_default_timezone_set('Asia/Manila');
			$current_date = date('d-m-y (h:i A)');
			echo '
				<div class="login-form" style="margin-top:50px">
					<form action="" method="post" style="background-color:light gray">
						<div class="text-center" style="color:black">Name: '.$_SESSION['name'].'</div>
						<div class="text-center" style="color:black">Year & Section: 3C</div>
						<div class="text-center" style="color:black">Date: '.$current_date.'</div>
					
						<hr>
				
						<h4 class="text-center" style="color:blue"><b>Welcome '.$_SESSION['oauth_provider'].'</b></h4>
						<form method="post"><input class="btn btn-danger btn-block login-btn" type="submit" value="Logout" name="logoutsr" ></form>
					</form>

				</div>
			';
		?>
	</body>
</html>