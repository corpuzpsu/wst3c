<?php
	session_start();
	if(isset($_SESSION['loginuser']))
	{
		header('Location: welcome.php');
	}
	else
	{
		session_unset();
	}
?>
<!DOCTYPE html>

<html lang="en">

	<head>
		<title>Login with Facebook/Google</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
		<link href="css/style.css" rel="stylesheet">

		<style>
		* {
			font-family: "Century Gothic", sans-serif;
			padding: 0px;
			box-sizing: border-box;
			margin: 0px;
		}
  
  		.login-form{
			width: 400px;
			margin: 5px auto;
			padding: 0px;
		}
  
		body {
			background: #1e1e1f;
		}
		</style>	

	</head>
	<body>
		<?php
			date_default_timezone_set('Asia/Manila');
			$current_date = date('d-m-y (h:i A)');
			echo '
			<div class="login-form" style="margin-top:50px">
				<form action="" method="post">
					<div class="text-center" style="color:black"> Name: Jackelyn N. Corpuz</div>
					<div class="text-center" style="color:black"> Year & Section: 3C</div>
					<div class="text-center" style="color:black"> Date: '.$current_date.'</div>
				</form>
			</div>';
			echo '
			<div class="login-form">
				<form action="" method="post">
					<a href="fb_login.php"><center><img src="images/fblogo.png" alt="Login with Facebook" width=260></center></a> 
				</form>
			</div>';

			include_once 'go_login.php';
			if(isset($_GET['code'])){
				$gClient->authenticate($_GET['code']);
				$_SESSION['token'] = $gClient->getAccessToken();
				header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));
			}
			if (isset($_SESSION['token'])) {
				$gClient->setAccessToken($_SESSION['token']);
			}
			if ($gClient->getAccessToken()) 
			{
				$gpUserProfile = $google_oauthV2->userinfo->get();
				$_SESSION['oauth_provider'] = 'Google'; 
				$_SESSION['name'] = $gpUserProfile['name']; 
				$_SESSION['loginuser']='yes';
			} else {
				$authUrl = $gClient->createAuthUrl();
				$output= 
				'<div class="login-form">
					<form action="" method="post">
						<a href="'.filter_var($authUrl, FILTER_SANITIZE_URL).'"><center><img src="images/gologo.png" alt="Sign in with Google+" width=260/></center></a>
					</form>
				</div>';
			}
			echo $output;
			
		?>	
	</body>
</html>