<?php
	session_start();
	require_once 'autoload.php';
	use Facebook\FacebookSession;
	use Facebook\FacebookRedirectLoginHelper;
	use Facebook\FacebookRequest;
	use Facebook\FacebookResponse;
	use Facebook\FacebookSDKException;
	use Facebook\FacebookRequestException;
	use Facebook\FacebookAuthorizationException;
	use Facebook\GraphObject;
	use Facebook\Entities\AccessToken;
	use Facebook\HttpClients\FacebookCurlHttpClient;
	use Facebook\HttpClients\FacebookHttpable;
	
	FacebookSession::setDefaultApplication( '514683239997272','2e1b5a4d759e73c444041500d294e6b4' );
	$helper = new FacebookRedirectLoginHelper('http://localhost/projects/fb-google-login/fb_login.php');
	
	try {
		$session = $helper->getSessionFromRedirect();
	} 
	catch( FacebookRequestException $ex ) {
	} 
	catch( Exception $ex ) {	
	}
	
	if ( isset( $session ) ) 
	{
		$request = new FacebookRequest( $session, 'GET', '/me?fields=id,first_name,last_name,name,email' );
		$response = $request->execute();
		$graphObject = $response->getGraphObject();
		$fbid = $graphObject->getProperty('id');
		$fbfullname = $graphObject->getProperty('name');

		$_SESSION['oauth_provider'] = 'Facebook'; 
		$_SESSION['oauth_uid'] = $fbid; 
		$_SESSION['name'] = $fbfullname; 
		$_SESSION['loginuser']='yes';
		header("Location: index.php");
	} 
	else 
	{
		$loginUrl = $helper->getLoginUrl();
		header("Location: ".$loginUrl);
	}
?>