<?php

    date_default_timezone_set('Asia/Manila');
    $current_date = date('d-m-y (h:i:s a)');
    $fname = 'Jackelyn';
    $lname = 'Corpuz';
    $year = 'Third Year';
    $section = 'C';
    $subject = 'Web Systems and Technologies 2';

    echo "<br>";
    echo "Name: <b> $fname $lname </b>";
    echo "<br>";
    echo "Year: <b> $year </b>";
    echo "<br>";
    echo "Section: <b> $section </b>";
    echo "<br>";
    echo "Subject: <b> $subject </b>";
    echo "<br>";
    echo "Current Date and Time: <b> $current_date </b>";

?>