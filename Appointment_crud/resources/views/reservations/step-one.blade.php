<x-app-layout>

        <div class="container w-full px-5 py-6 mx-auto">
            <div class="flex items-center min-h-screen bg-gray-50">
                <div class="flex-1 h-full max-w-4xl mx-auto shadow-xl">
                    <div class="flex flex-col md:flex-row">
                        <div class="h-30 md:h-auto md:w-1/2">
                            <img class="object-cover w-full h-full"
                                src="https://i.pinimg.com/originals/1e/0e/a8/1e0ea8283c08f9bb9d40d8a10ae0485c.jpg" alt="img" />
                        </div>
                        <div class="flex items-center justify-center p-6 sm:p-12 md:w-1/2">
                            <div class="w-full">
                                <h3 class="flex items-center justify-center font-sans mb-8 text-2xl font-bold text-transparent bg-clip-text bg-gradient-to-r from-pink-600 to-gray-700">
                                    MAKE AN APPOINTMENT</h3>
    
                                <form method="POST" action="{{ route('reservations.store.step.one') }}">
                                    @csrf
                                    <div class="sm:col-span-6">
                                        <label for="first_name" class="block text-sm font-medium text-gray-700"> First Name
                                        </label>
                                        <div class="mt-1">
                                            <input type="text" id="first_name" name="first_name"
                                                value="{{ $reservation->first_name ?? '' }}"
                                                class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                        </div>
                                        @error('first_name')
                                            <div class="text-sm text-red-400">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="sm:col-span-6">
                                        <label for="last_name" class="block text-sm font-medium text-gray-700"> Last Name
                                        </label>
                                        <div class="mt-1">
                                            <input type="text" id="last_name" name="last_name"
                                                value="{{ $reservation->last_name ?? '' }}"
                                                class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                        </div>
                                        @error('last_name')
                                            <div class="text-sm text-red-400">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="sm:col-span-6">
                                        <label for="email" class="block text-sm font-medium text-gray-700"> Email </label>
                                        <div class="mt-1">
                                            <input type="email" id="email" name="email"
                                                value="{{ $reservation->email ?? '' }}"
                                                class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                        </div>
                                        @error('email')
                                            <div class="text-sm text-red-400">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="sm:col-span-6">
                                        <label for="phone" class="block text-sm font-medium text-gray-700"> Phone
                                            number
                                        </label>
                                        <div class="mt-1">
                                            <input type="number" id="phone" name="phone"
                                                value="{{ $reservation->phone ?? '' }}"
                                                class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                        </div>
                                        @error('phone')
                                            <div class="text-sm text-red-400">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="sm:col-span-6">
                                        <label for="res_date" class="block text-sm font-medium text-gray-700"> Booking
                                            Date
                                        </label>
                                        <div class="mt-1">
                                            <input type="datetime-local" id="res_date" name="res_date"
                                                min="{{ $min_date->format('Y-m-d\TH:i:s') }}"
                                                max="{{ $max_date->format('Y-m-d\TH:i:s') }}"
                                                value="{{ $reservation ? $reservation->res_date->format('Y-m-d\TH:i:s') : '' }}"
                                                class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                        </div>
                                        <span class="text-xs italic">Please choose the time between 8:00AM - 5:00PM.</span>
                                        @error('res_date')
                                            <div class="text-sm text-red-400">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="sm:col-span-6">
                                        <label for="service" class="block text-sm font-medium text-gray-700"> Service
                                        </label>
                                        <div class="mt-1">
                                            <input type="text" id="service" name="service"
                                                value="{{ $reservation->service ?? '' }}"
                                                class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                        </div>
                                        @error('service')
                                            <div class="text-sm text-red-400">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mt-6 p-4 flex justify-center">
                                        <button type="submit"
                                        class="px-4 py-2 bg-pink-600 hover:bg-pink-400 rounded-lg text-white">Book Now</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
        </div>

</x-app-layout>
