<x-guest-layout>

  <section id="home"
    class="container max-w-lg px-4 py-32 mx-auto text-left bg-center bg-no-repeat bg-fixed bg-cover md:max-w-none md:text-center"
    style="background-image: url('https://img5.goodfon.com/wallpaper/nbig/6/ad/tender-white-flowers-pink-background-rozovyi-fon-belye-tsvet.jpg')">  
    <br>
    <h1
      class="font-serif text-5xl font-extrabold tracking-wide text-transparent sm:text-5xl md:text-5xl lg:text-5xl xl:text-6xl bg-clip-text bg-gradient-to-r from-pink-600 to-gray-800 md:text-center sm:leading-none">
      <span class="inline md:block ">GG <br> BEAUTY SALON</span>
    </h1>
    <div class="mx-auto mt-3 tracking-wide italic text-gray-800 md:text-center font-medium lg:text-lg drop-shadow-2xl no-underline">
      We want to settle on a style that suits the image of <br>yourself you want to have.
    </div>
    <div class="flex flex-col items-center mt-12 text-center">
      <span class="relative inline-flex w-full md:w-auto">
        <a href="{{ route('reservations.step.one') }}" type="button"
          class="inline-flex items-center trecking-wide justify-center px-6 py-2 text-base font-medium leading-6 text-white bg-pink-600 shadow-lg shadow-pink-900/50 rounded-full lg:w-full md:w-auto hover:bg-gray-800 focus:outline-none">
          MAKE AN APPOINTMENT
        </a>
    </div>
    <br>
  </section>

</x-guest-layout>